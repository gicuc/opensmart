# OpenSMART - SMART++ (by ATC - University of Cantabria)

This is a custom version of OpenSMART ver. 1.0, provided by Hyoukjun at [Synergy lab](http://synergy.ece.gatech.edu), to implement SMART++.
Besides implementing SMART++, this version also fixes some issues of OpenSMART v1.0 like packet losses and miss-routing.

This tool is part of [BST](https://www.atc.unican.es/software.html) for the purposes of model validating and area and power measuring.

>## OpenSMART ver. 1.0 Release
>
>We have released the backend of OpenSMART, which is the core implementation in Bluespec and Chisel. We are developing the front-end which can automate the network generation. The front-end features will include DOT topology file and configuration file support, and unified executable for compilation and simulation. Please check the website for latest news about that.
>
>## Documentation
>
>We are preparing more detailed documentation that describes module interfaces and behaviors of each module. We will also announce it to this page when it is available.
>For further information, please refer to the [paper](http://synergy.ece.gatech.edu/wp-content/uploads/sites/332/2017/03/OpenSMART_ISPASS17.pdf)
>
>### Contact
>If you have questions about the code, please contact Hyoukjun (hyoukjun [at] gatech [dot] edu).
>[Synergy lab](http://synergy.ece.gatech.edu)
