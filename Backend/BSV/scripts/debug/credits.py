#!/bin/python3
import sys
import re

def main():
	
	filename = sys.argv[1]
	
	routers = [4,4]
	ports = 5
	
	#send_flit_router = re.compile(r".*(\d+) \| top.meshNtk.routers_(\d)_(\d) - ActionValue getFlit \| prt: (\d), dstPrt: \d+, smartFlags[prt].isPass: \d")
	#get_credit = re.compile(r".*(\d+) \| top.meshNtk.routers_(\d)_(\d).vcAllocUnits_(\d) - Action putFreeVC(VCidx: \d)")
	send_flit_router = re.compile(r"(\d+) \| top\.meshNtk\.routers_(\d)_(\d) - ActionValue getFlit \| prt: (\d), dstPrt: \d+, smartFlags\[prt\]\.isPass: \d")
	get_credit = re.compile(r"(\d+) \| top\.meshNtk\.routers_(\d)_(\d)\.vcAllocUnits_(\d) - Action putFreeVC\(VCidx: \d\)")
	
	#pending_credits = [[[False] * ports]*routers[0]]*routers[1] # Indexation: [y][x][port] FIXME
	pending_credits = [[[False for port in range(ports)] for x in range(routers[1])] for y in range(routers[0])] # Indexation: [y][x][port] FIXME
	
	#print("Pending credits: {}".format(pending_credits))
	
	# Read file
	f = open(filename, 'r') 
	for line in f.readlines():
		#send_flit_res = send_flit_router.findall(line)
		#get_credit_res = get_credit.findall(line)
		send_flit_res = send_flit_router.search(line)
		get_credit_res = get_credit.search(line)
		if send_flit_res:
			y, x, port = int(send_flit_res.group(2)), int(send_flit_res.group(3)), int(send_flit_res.group(4))
			if pending_credits[y][x][port] == False:
				pending_credits[y][x][port] = True
				print("Tracking Credit. Cycle: {}, router: y_{} x_{} port_{}".format(send_flit_res.group(1), y, x, port))
			else:
				print("Warning pending credits is already True: {}".format(send_flit_res.group(0)))
			
		if get_credit_res:
			y, x, port = int(get_credit_res.group(2)), int(get_credit_res.group(3)), int(get_credit_res.group(4))
			if pending_credits[y][x][port] == True:
				print("Correct credit realease: Cycle: {}, router: y_{} x_{} port_{}".format(get_credit_res.group(1), y, x, port))
				pending_credits[y][x][port] = False
			else:
				print("Warning pending credits is already False: {}".format(get_credit_res.group(0)))
	f.close()
	
	for y, x, port in zip(range(routers[0]), range(routers[1]), range(ports)):
		if pending_credits[y][x][port]:
			print("Warning: router_{}_{} port: {} still has a credit pending".format(y,x,port))
	
	
	
	
# Call to main function
main()
