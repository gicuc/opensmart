#!/bin/python3
import sys
import re

def main():

    filename = sys.argv[1]
    nodes = int(sys.argv[2])
    k = int(sys.argv[3])

    create_flit = re.compile(r"\d+ \| top\.trafficGeneratorUnits_\d+_\d+ - Action genFlit \| .*ID:.* (\d+), Source: y_(\d+) x_(\d+).*")
    receive_flit = re.compile(r"Correct - Flit.* (\d+) - Received .* source: \((\d+), (\d+)\)")

    flits = [set() for x in range(nodes)]

    # Read file
    f = open(filename, 'r') 
    for line in f.readlines():
        #send_flit_res = send_flit_router.findall(line)
        #get_credit_res = get_credit.findall(line)
        create_flit_res = create_flit.search(line)
        receive_flit_res = receive_flit.search(line)
        if create_flit_res:
            fid, y, x = int(create_flit_res.group(1)), int(create_flit_res.group(2)), int(create_flit_res.group(3))
            print("Flit: {}, Source: y_{} x_{} ({}) generated".format(fid, y, x, y*4+x))
            flits[y*k+x].add(fid)
        if receive_flit_res:
            fid, y, x = int(receive_flit_res.group(1)), int(receive_flit_res.group(2)), int(receive_flit_res.group(3))
            print("Flit: {}, Source: y_{} x_{} ({}) consumed".format(fid, y, x, y*4+x))
            flits[y*k+x].remove(fid)
    f.close()

    router = 0
    for s in flits:
        print("Router: y_{} x_{} | Not received flits: {}".format(int(router/k), router%k, s))
        router += 1


# Call to main function
main()
