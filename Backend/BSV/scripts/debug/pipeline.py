#!/bin/python3
import sys

def main():

    filename = sys.argv[1]

    router = "routers_0_0"
    
    pipeline_rules = list() # element: pair of cycle and list of rules

    # rules to look for in the file
    possible_rules = ["rl_ReqLocalSA",
                     "rl_RespLocalSA",
                     "rl_GetLocalSARes",
                     "rl_deqTempBufs",
                     "rl_SendSSR",
                     "rl_GlobalSA",
                     "rl_deqFlags",
                     "rl_PrepareLocalFlits",
                     "rl_enqOutBufs",
                     "getFlit",
                     "putFlit"
                    ] 

    # Read file
    f = open(filename, 'r') 
    cycle = 0 # Track last cycle
    rules = []
    for line in f.readlines():
        current_cycle = int(int(line.split()[0])/10) if line.split()[0].isdigit() else current_cycle
        if(current_cycle > cycle):
            pipeline_rules.append((cycle, rules.copy()))
            cycle = current_cycle
            rules = []
        for rule in possible_rules:
            if rule in line and router in line:
                rules.append(rule)
    f.close()

    #print("Rules list:")
    #[print("\tCycle: {} |\trules: {}".format(cycle,rules)) for cycle, rules in pipeline_rules]
    cycle_counter = pipeline_rules[0][0]
    for cycle, rules in pipeline_rules:
        if(cycle_counter < cycle):
            print("Jumped {} cycles in time.".format(cycle-cycle_counter))
            cycle_counter = cycle

        if(len(rules) < len(possible_rules)):
            print("Missing rules in cycle: {} |\tMissing Rules: {}\n\t\t\t\tRules: {}".format(cycle_counter, list(set(possible_rules) - set(rules)), rules))
        else:
            print("Every rule in cycle: {} |\tRules: {}".format(cycle_counter, rules))

        cycle_counter += 1
    
    print
    
    


# Call to main function
main()
