#!/bin/python3

# Author: Ivan Perez Gallardo
# Affiliation: University of Cantabria
# Date: 2019/02/22

# Parses the output of OpenSMART (first argument of the script)
# and returns (stdcout) a csv file with stats.
# The second argument is the number of nodes

import re
import sys

filename = sys.argv[1]
nodes = int(sys.argv[2])

header = [
                "class",
                "avg_sent_packet_size",
		"load",
		"sent_flits",
		"recv_flits",
		"avg_accepted_flits",
		"avg_injected_flits",
		"avg_plat",
		"avg_nlat",
		"smart_hops"
		]

# XXX: I look for the beginning of the block with stats.
header_str = ','.join(header)
print(header_str)
with open(filename, "r") as f:
    for line in f:
        if "Elapsed clock cycles:" in line:
            ECC = int(line.split(":")[1])
            # Read next line: Injection rate
            IR = int(f.readline().split(":")[1])
            # Read next line: Total injected packet
            TIP = int(f.readline().split(":")[1])
            # Read next line: Total received packet
            TRP = int(f.readline().split(":")[1])
            # Read next line: Total latency
            TL = int(f.readline().split(":")[1])
            # Read next line: Total hopCount
            THC = int(f.readline().split(":")[1])
            # Read next line: Total inflight latency
            TIL = int(f.readline().split(":")[1])
            
            # Eval new stats:
            # load
            class_f = 0 # FIXME: Add this field to OpenSMART packets
            avg_sent_packet_size = 1
            load = IR / 100
            sent_flits = TIP
            recv_flits = TRP
            accepted_load = TRP / ECC / nodes
            injected_load = TIP / ECC / nodes
            avg_plat = TL / TRP
            avg_nlat = TIL / TRP
            smart_hops = THC / TRP
            out = [class_f, avg_sent_packet_size, load, sent_flits, recv_flits, accepted_load, injected_load, avg_plat, avg_nlat, smart_hops]
            out_str = [str(x) for x in out]
            print(','.join(out_str))


