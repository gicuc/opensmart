#!/bin/bash

template_file=scripts/template/Types_template.bsv

date=`date +"%Y_%m_%d"`
output_dir=output/${date}

mkdir ${output_dir};

for vcs in {1,2,4,8};
do
	#for bufsize in {1,2,4,8};
	for bufsize in {1,2,4,8};
	do
		echo "" > /tmp/sim_out;

		for ir in `seq -w 01 02 50`;
		do
			cp ${template_file} src/Types/Types.bsv;
			echo "IR = ${ir} | VCs = ${vcs} | Buf. Size = ${bufsize} | /usr/bin/time -v ./OpenSMART -c SMART &> /tmp/comp_out_${new_name}; ./build/sim > /tmp/sim_out_${new_name};"
			sed -i "s/typedef XX InjectionRate;/typedef ${ir} InjectionRate;/g" src/Types/Types.bsv;
			sed -i "s/typedef XX NumUserVCs;/typedef ${vcs} NumUserVCs;/g" src/Types/Types.bsv;
			sed -i "s/typedef XX MaxVCDepth;/typedef ${bufsize} MaxVCDepth;/g" src/Types/Types.bsv;
			/usr/bin/time -v ./OpenSMART -c SMART &> /tmp/comp_out; ./build/sim >> /tmp/sim_out;
		done
		python3 scripts/output_parser.py /tmp/sim_out 64 > ${output_dir}/smart_nebb_8x8_hpcmax_8_vc_${vcs}_buf_size_${bufsize}.out;
	done
done

