#!/bin/bash


for file in scripts/templates/Types_{1,2,4,8}.bsv;
do
	new_name=${file#"scripts/template/Types_"};
	new_name=${new_name%".bsv"};
	echo ${file}
	cp ${file} src/Types/Types.bsv;
	echo "/usr/bin/time -v ./OpenSMART -c SMART &> /tmp/comp_out_${new_name}; ./build/sim > /tmp/sim_out_${new_name};"
	/usr/bin/time -v ./OpenSMART -c SMART &> /tmp/comp_out_${new_name}; ./build/sim > /tmp/sim_out_${new_name};
	python3 scripts/output_parser.py /tmp/sim_out_${new_name} 16 > output/2019_apr_10_nebb_smart_4x4_vc_2_hpcmax_4_buf_size_${new_name}.out;
done

