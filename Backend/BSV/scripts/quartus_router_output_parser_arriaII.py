#!/bin/python3

# Author: Ivan Perez Gallardo
# Affiliation: University of Cantabria
# Date: 2019/05/21

# TODO: description of the script

import re
import sys
import os
import math

def sorted_aphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

# Read alms
def extract_alms(sim):
    filename = "{}/RouterTestBench.fit.rpt".format(sim)
    alms_network = 0.0  #total dynamic power 
    alms_input_units = 0.0  #total input units 
    alms_sa_l = 0.0
    alms_cb = 0.0
    alms_crd_units = 0.0
    alms_vca = 0.0
    
    entity_block = False
    with open(filename, "r", encoding='utf-8') as f:
        for line in f:
            fields = line.split(";")
            if "; Fitter Resource Utilization by Entity" in line:
                entity_block = True

            if entity_block:
                if len(fields) < 2:
                    continue

                if "|mkSmartRouter:router" in fields[1]:
                    alms_network += float(fields[5].split(" (")[0])
                
                if "|mkInputUnit:inputUnits_" in fields[1]:
                    alms_input_units += float(fields[5].split(" (")[0])
                
                if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                    alms_sa_l += float(fields[5].split(" (")[0])
                if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                    alms_sa_l += float(fields[5].split(" (")[0])
                if "|FIFOL1:pipe_headers_" in fields[1]:
                    alms_sa_l += float(fields[5].split(" (")[0])
                
                if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                    alms_cb += float(fields[5].split(" (")[0])
                if "|FIFOL1:sa2cb_" in fields[1]:
                    alms_cb += float(fields[5].split(" (")[0])
                
                if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                    alms_crd_units += float(fields[5].split(" (")[0])
                
                if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                    alms_vca += float(fields[5].split(" (")[0])

                if "; Delay Chain Summary" in line:
                    entity_block = False

    result = [alms_network, alms_input_units, alms_sa_l, alms_cb, alms_crd_units,alms_vca]
    return result

def extract_alus(sim):
    filename = "{}/RouterTestBench.fit.rpt".format(sim)
    alus_network = 0.0  #total dynamic power 
    alus_input_units = 0.0  #total input units 
    alus_sa_l = 0.0
    alus_cb = 0.0
    alus_crd_units = 0.0
    alus_vca = 0.0
    
    entity_block = False
    with open(filename, "r") as f:
        for line in f:
            fields = line.split(";")
            if "; Fitter Resource Utilization by Entity" in line:
                entity_block = True

            if entity_block:
                if len(fields) < 2:
                    continue

                if "|mkSmartRouter:router" in fields[1]:
                    alus_network += float(fields[2].split(" (")[0])
                
                if "|mkInputUnit:inputUnits_" in fields[1]:
                    alus_input_units += float(fields[2].split(" (")[0])
                
                if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                    alus_sa_l += float(fields[2].split(" (")[0])
                if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                    alus_sa_l += float(fields[2].split(" (")[0])
                if "|FIFOL1:pipe_headers_" in fields[1]:
                    alus_sa_l += float(fields[2].split(" (")[0])
                
                if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                    alus_cb += float(fields[2].split(" (")[0])
                if "|FIFOL1:sa2cb_" in fields[1]:
                    alus_cb += float(fields[2].split(" (")[0])
                
                if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                    alus_crd_units += float(fields[2].split(" (")[0])
                
                if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                    alus_vca += float(fields[2].split(" (")[0])

                if "; Delay Chain Summary" in line:
                    entity_block = False

    result = [alus_network, alus_input_units, alus_sa_l, alus_cb, alus_crd_units,alus_vca]
    return result

def extract_regs(sim):
    filename = "{}/RouterTestBench.fit.rpt".format(sim)
    regs_network = 0.0  #total dynamic power 
    regs_input_units = 0.0  #total input units 
    regs_sa_l = 0.0
    regs_cb = 0.0
    regs_crd_units = 0.0
    regs_vca = 0.0
    
    entity_block = False
    with open(filename, "r") as f:
        for line in f:
            fields = line.split(";")
            if "; Fitter Resource Utilization by Entity" in line:
                entity_block = True

            if entity_block:
                if len(fields) < 2:
                    continue

                if "|mkSmartRouter:router" in fields[1]:
                    regs_network += float(fields[6].split(" (")[0])
                
                if "|mkInputUnit:inputUnits_" in fields[1]:
                    regs_input_units += float(fields[6].split(" (")[0])
                
                if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                    regs_sa_l += float(fields[6].split(" (")[0])
                if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                    regs_sa_l += float(fields[6].split(" (")[0])
                if "|FIFOL1:pipe_headers_" in fields[1]:
                    regs_sa_l += float(fields[6].split(" (")[0])
                
                if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                    regs_cb += float(fields[6].split(" (")[0])
                if "|FIFOL1:sa2cb_" in fields[1]:
                    regs_cb += float(fields[6].split(" (")[0])
                
                if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                    regs_crd_units += float(fields[6].split(" (")[0])
                
                if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                    regs_vca += float(fields[6].split(" (")[0])

                if "; Delay Chain Summary" in line:
                    entity_block = False

    result = [regs_network, regs_input_units, regs_sa_l, regs_cb, regs_crd_units,regs_vca]
    return result

def extract_mem(sim):
    filename = "{}/RouterTestBench.fit.rpt".format(sim)
    mem_network = 0.0  #total dynamic power 
    mem_input_units = 0.0  #total input units 
    mem_sa_l = 0.0
    mem_cb = 0.0
    mem_crd_units = 0.0
    mem_vca = 0.0
    
    entity_block = False
    with open(filename, "r", encoding='utf-8') as f:
        for line in f:
            fields = line.split(";")
            if "; Fitter Resource Utilization by Entity" in line:
                entity_block = True

            if entity_block:
                if len(fields) < 2:
                    continue

                if "|mkSmartRouter:router" in fields[1]:
                    mem_network += float(fields[8].split(" (")[0])
                
                if "|mkInputUnit:inputUnits_" in fields[1]:
                    mem_input_units += float(fields[8].split(" (")[0])
                
                if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                    mem_sa_l += float(fields[8].split(" (")[0])
                if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                    mem_sa_l += float(fields[8].split(" (")[0])
                if "|FIFOL1:pipe_headers_" in fields[1]:
                    mem_sa_l += float(fields[8].split(" (")[0])
                
                if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                    mem_cb += float(fields[8].split(" (")[0])
                if "|FIFOL1:sa2cb_" in fields[1]:
                    mem_cb += float(fields[8].split(" (")[0])
                
                if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                    mem_crd_units += float(fields[8].split(" (")[0])
                
                if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                    mem_vca += float(fields[8].split(" (")[0])

                if "; Delay Chain Summary" in line:
                    entity_block = False

    result = [mem_network, mem_input_units, mem_sa_l, mem_cb, mem_crd_units,mem_vca]
    return result


# Read fmax
def extract_frequency(sim):
    filename = "{}/RouterTestBench.sta.rpt".format(sim)
    fmax_85 = "-1"
    fmax_0  = "-1"
    with open(filename, "r") as f:
        for line in f:
            if "; Slow 900mV 85C Model Fmax Summary" in line :
                f.readline()
                f.readline()
                f.readline()
                rline = f.readline()
                fmax_85 = rline.split(";")[1].replace("MHz", "").replace(" ","")
            
            if "; Slow 900mV 0C Model Fmax Summary" in line:
                f.readline()
                f.readline()
                f.readline()
                rline = f.readline()
                fmax_0 = rline.split(";")[1].replace("MHz", "").replace(" ","")

    result = [fmax_85, fmax_0]
    return result

# Read power
def extract_powers(sim):
    filename = "{}/RouterTestBench.pow.rpt".format(sim)
    dp_network = 0.0  #total dynamic power 
    dp_input_units = 0.0  #total input units 
    dp_sa_l = 0.0
    dp_cb = 0.0
    dp_crd_units = 0.0
    dp_vca = 0.0
    with open(filename, "r") as f:
        for line in f:
            fields = line.split(";")
            if len(fields) < 2:
                continue

            if "|mkSmartRouter:router" in fields[1]:
                dp_network += float(fields[2].split(" mW")[0])
            
            if "|mkInputUnit:inputUnits_" in fields[1]:
                dp_input_units += float(fields[2].split(" mW")[0])
            
            if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            if "|FIFOL1:pipe_headers_" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            
            if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                dp_cb += float(fields[2].split(" mW")[0])
            if "|FIFOL1:sa2cb_" in fields[1]:
                dp_cb += float(fields[2].split(" mW")[0])
            
            if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                dp_crd_units += float(fields[2].split(" mW")[0])
            
            if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                dp_vca += float(fields[2].split(" mW")[0])
            

    result = [dp_network, dp_input_units, dp_sa_l, dp_cb, dp_crd_units,dp_vca]
    return result





sim_dir = sys.argv[1]
nodes = int(sys.argv[2])
k = math.sqrt(nodes)

header = [
                "config",
                "fmax_slow_85c_mhz",
                "fmax_slow_0c_mhz",
                "dp_network", #total dynamic power 
                "dp_input_units", #total input units 
                "dp_sa-l",
                "dp_cb",
                "dp_crd_units",
                "dp_vca",
                "alms_network", #total dynamic power 
                "alms_input_units", #total input units 
                "alms_sa-l",
                "alms_cb",
                "alms_crd_units",
                "alms_vca",
                "alus_network", #total dynamic power 
                "alus_input_units", #total input units 
                "alus_sa-l",
                "alus_cb",
                "alus_crd_units",
                "alus_vca",
                "reg_network", #total dynamic power 
                "reg_input_units", #total input units 
                "reg_sa-l",
                "reg_cb",
                "reg_crd_units",
                "reg_vca",
                "mem_network", #total dynamic power 
                "mem_input_units", #total input units 
                "mem_sa-l",
                "mem_cb",
                "mem_crd_units",
                "mem_vca",
		]

# XXX: I look for the beginning of the block with stats.
header_str = ','.join(header)
print(header_str)

simulations = sorted_aphanumeric(os.listdir(sim_dir))

for sim in simulations:
    sim_values = [sim]
    

    temp = extract_frequency(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(val)

    temp = extract_powers(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))
    
    temp = extract_alms(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))

    temp = extract_alus(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))
    
    temp = extract_regs(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))
    
    temp = extract_mem(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))
    
    sim_str = ','.join(sim_values)
    print(sim_str)

