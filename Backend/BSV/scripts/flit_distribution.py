#!/bin/python3

# Author: Ivan Perez Gallardo
# Affiliation: University of Cantabria
# Date: 2019/02/22

# Parses the output of OpenSMART (first argument of the script)
# and returns (stdcout) a csv file with stats.
# The second argument is the number of nodes

import re
import sys
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

filename = sys.argv[1]
nodes = int(sys.argv[2])
outfile = sys.argv[3]

fig1 = plt.figure(figsize=(10,100))
ax = list()
gs = gridspec.GridSpec(25, 2)
gs.update(hspace=0.4)

row = 0
with open(filename, "r") as f:
    for line in f:
        injected_flits = list()
        consumed_flits = list()
        if "send_count" in line:
            line_split = line.split(",")
            send_count  = int(line_split[0].split("=")[1])
            recv_count  = int(line_split[1].split("=")[1])
            injected_flits.append(send_count)
            consumed_flits.append(recv_count)

            for i in range(nodes-1):
                new_line = f.readline() 
                line_split = new_line.split(",")
                send_count  = int(line_split[0].split("=")[1])
                recv_count  = int(line_split[1].split("=")[1])
                injected_flits.append(send_count)
                consumed_flits.append(recv_count)
                
                
            ECC = int(f.readline().split(":")[1])
            IR = int(f.readline().split(":")[1])
            
            print('Injection Rate: {}\n\tinjected_flits: {}\n\tconsumed_flits: {}'.format(IR, injected_flits, consumed_flits))

            ax.append(fig1.add_subplot(gs[row,0]))
            ax[-1].set_title('Injection rate = %s' % str(IR))
            ax[-1].set_ylabel('Injected flits')
            ax[-1].bar(range(len(injected_flits)), injected_flits)
            ax.append(fig1.add_subplot(gs[row,1]))
            ax[-1].set_title('Injection rate = %s' % str(IR))
            ax[-1].set_ylabel('Consumed flits')
            ax[-1].bar(range(len(consumed_flits)), consumed_flits)

            row += 1

plt.savefig(outfile)

