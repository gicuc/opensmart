#!/bin/python3

# NOTE: Simulate using watch_every_packet=1 watch_out=<output file>.
# NOTE: Compile with -DPIPELINE_DEBUG

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="input file generated from a BookSim simulation")
parser.add_argument("outputfile", help="output file with a csv format representing the pipeline of the routers in the simulation")

args = parser.parse_args()

# FIXME: get automatically this value
num_routers = 16
num_routers = 4
#num_routers = 2
ports = 5
vc_buf_size = 8
start_cycle = 1000
end_cycle = 2000

router_dict = {"top.meshNtk.routers_0_0" : 0,
               "top.meshNtk.routers_0_1" : 1,    
               "top.meshNtk.routers_0_2" : 2,    
               "top.meshNtk.routers_0_3" : 3,    
               "top.meshNtk.routers_1_0" : 4,    
               "top.meshNtk.routers_1_1" : 5,    
               "top.meshNtk.routers_1_2" : 6,    
               "top.meshNtk.routers_1_3" : 7,    
               "top.meshNtk.routers_2_0" : 8,    
               "top.meshNtk.routers_2_1" : 9,    
               "top.meshNtk.routers_2_2" : 10,    
               "top.meshNtk.routers_2_3" : 11,    
               "top.meshNtk.routers_3_0" : 12,    
               "top.meshNtk.routers_3_1" : 13,    
               "top.meshNtk.routers_3_2" : 14,    
               "top.meshNtk.routers_3_3" : 15,    
        }

outport_dict = {"Output 0" : 0,
                "Output 1" : 1,
                "Output 2" : 2,
                "Output 3" : 3,
                "Output 4" : 4,
        }

inport_dict = {"Input 0" : 0,
               "Input 1" : 1,
               "Input 2" : 2,
               "Input 3" : 3,
               "Input 4" : 4,
        }

credits_avail = [[vc_buf_size for x in range(ports)] for y in range(num_routers)]
credits_local = [[vc_buf_size for x in range(ports)] for y in range(num_routers)]
credits_bypass = [[vc_buf_size for x in range(ports)] for y in range(num_routers)]
credits_reception = [[vc_buf_size for x in range(ports)] for y in range(num_routers)]
BW = [["X" for x in range(ports)] for y in range(num_routers)]
sal = [["X" for x in range(ports)] for y in range(num_routers)]
sal_fail = [["X" for x in range(ports)] for y in range(num_routers)]
sag = [["X" for x in range(ports)] for y in range(num_routers)]
st_lt = [["X" for x in range(ports)] for y in range(num_routers)]

cycle = -1

output_stream = list()

temp = ""
# Read file
f = open(args.inputfile, 'r') 
for line in f.readlines():

    line_split = line.split(" | ")
    
    if len(line_split) < 3:
        continue

    if cycle != int(line_split[0])/10:
        if cycle >= start_cycle and cycle <= end_cycle:
            temp = list()

            for r in range(num_routers):
                for i in range(ports):
                    temp.append(BW[r][i])
                    temp.append(credits_local[r][i])
                    temp.append(credits_bypass[r][i])
                for o in range(ports):
                    temp.append(sal[r][o])
                    temp.append(sal_fail[r][o])
                    temp.append(sag[r][o])
                    temp.append(st_lt[r][o])
                    temp.append(credits_reception[r][o])
                    temp.append(credits_avail[r][o])
            output_stream.append(temp)

            credits_avail = [[vc_buf_size for x in range(ports)] for y in range(num_routers)]
            credits_local = [["X" for x in range(ports)] for y in range(num_routers)]
            credits_bypass = [["X" for x in range(ports)] for y in range(num_routers)]
            credits_reception = [["X" for x in range(ports)] for y in range(num_routers)]
            BW = [["X" for x in range(ports)] for y in range(num_routers)]
            sal = [["X" for x in range(ports)] for y in range(num_routers)]
            sal_fail = [["X" for x in range(ports)] for y in range(num_routers)]
            sag = [["X" for x in range(ports)] for y in range(num_routers)]
            st_lt = [["X" for x in range(ports)] for y in range(num_routers)]

        cycle = int(line_split[0])/10

    if line_split[2] == "Credit availability":
        router_output = line_split[1].split(".vcAllocUnits_")
        if router_output[0] in router_dict:
            credits_avail[router_dict[router_output[0]]][int(router_output[1])] = line_split[4].replace("\n","").replace(" ","")
    
    if line_split[2] == "Credit Local":
        credits_local[router_dict[line_split[1]]][int(line_split[4].replace("Input","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "Credit Bypass":
        credits_bypass[router_dict[line_split[1]]][int(line_split[4].replace("Input","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "Credit Reception":
        credits_reception[router_dict[line_split[1]]][int(line_split[4].replace("\n","").replace("Output","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "BW":
        BW[router_dict[line_split[1]]][int(line_split[4].replace("Input","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ", "")

    if line_split[2] == "SA-L":
        sal[router_dict[line_split[1]]][int(line_split[5].replace("Output","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "No free VC":
        router_name = line_split[1].replace(".localSAUnit","") 
        sal_fail[router_dict[router_name]][int(line_split[5].replace("Output","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "SA-G":
        sag[router_dict[line_split[1]]][int(line_split[5].replace("Output","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")
    
    if line_split[2] == "ST+LT":
        st_lt[router_dict[line_split[1]]][int(line_split[5].replace("Output","").replace(" ",""))] = line_split[3].replace("Flit", "").replace(" ","")

f.close()
len_y = len(output_stream[0]) 
len_x = len(output_stream)

cycle = start_cycle
temp = "Router; "
for y in range(len_y):
    temp += str(cycle) + "; "
    cycle += 1
print(temp)
            
header = list()
for r in range(num_routers):
    for i in range(ports):
        header.append("R{} BW I{}".format(r, i))
        header.append("R{} Crd L I{}".format(r, i))
        header.append("R{} Crd B I{}".format(r, i))
    for o in range(ports):
        header.append("R{} SA-L O{}".format(r, o))
        header.append("R{} SA-L (MISS) O{}".format(r, o))
        header.append("R{} SA-G O{}".format(r, o))
        header.append("R{} ST+LT O{}".format(r, o))
        header.append("R{} Crd Rec O{}".format(r, o))
        header.append("R{} Credits O{}".format(r, o))

for y in range(len_y):
    temp = header[y] + "; " 
    for x in range(len_x):
        temp += str(output_stream[x][y]) + "; "
    print(temp)

