#!/bin/python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="input file generated from a BookSim simulation")
parser.add_argument("sim", help="booksim or bluespec")

args = parser.parse_args()

# FIXME: get automatically this value
#num_routers = 16
num_routers = 4
#num_routers = 2
ports = 5
vc_buf_size = 8
start_cycle = 0
end_cycle = 1000

f = open(args.inputfile, 'r')
#time = f.readline().split(";")[1:-1]

r0_credits = list()
r1_credits = list()
r2_credits = list()
r0_1_lt = list()
r1_2_lt = list()
r2_node_lt = list()
r1_0_cr = list()
r2_1_cr = list()
node_r2_cr = list()

if args.sim == "booksim":
    for line in f.readlines():
        line_splitted = line.split(";")
        if "R0 Credits O0" in line_splitted[0]:
            r0_credits = [vc_buf_size - int(x) for x in line_splitted[1:-1]]
        if "R1 Credits O0" in line_splitted[0]:
            r1_credits = [vc_buf_size - int(x) for x in line_splitted[1:-1]]
        if "R2 Credits O4" in line_splitted[0]:
            r2_credits = [vc_buf_size - int(x) for x in line_splitted[1:-1]]
        if "R0 ST+LT O0" in line_splitted[0]:
            r0_1_lt = line_splitted[1:-1]
        if "R1 ST+LT O0" in line_splitted[0]:
            r1_2_lt = line_splitted[1:-1]
        if "R2 ST+LT O4" in line_splitted[0]:
            r2_node_lt = line_splitted[1:-1]
        if "R0 Crd Rec O0" in line_splitted[0]:
            r1_0_cr = line_splitted[1:-1]
        if "R1 Crd Rec O0" in line_splitted[0]:
            r2_1_cr = line_splitted[1:-1]
        if "R2 Crd Rec O4" in line_splitted[0]:
            node_r2_cr = line_splitted[1:-1]

elif args.sim == "bluespec":
    for line in f.readlines():
        line_splitted = line.split(";")
        if "R0 Credits O1" in line_splitted[0]:
            r0_credits = line_splitted[1:-1]
        if "R1 Credits O1" in line_splitted[0]:
            r1_credits = line_splitted[1:-1]
        if "R2 Credits O4" in line_splitted[0]:
            r2_credits = line_splitted[1:-1]
        if "R0 ST+LT O1" in line_splitted[0]:
            r0_1_lt = line_splitted[1:-1]
        if "R1 ST+LT O1" in line_splitted[0]:
            r1_2_lt = line_splitted[1:-1]
        if "R2 ST+LT O4" in line_splitted[0]:
            r2_node_lt = line_splitted[1:-1]
        if "R0 Crd Rec O1" in line_splitted[0]:
            r1_0_cr = line_splitted[1:-1]
        if "R1 Crd Rec O1" in line_splitted[0]:
            r2_1_cr = line_splitted[1:-1]
        if "R2 Crd Rec O4" in line_splitted[0]:
            node_r2_cr = line_splitted[1:-1]


header = "time;R0 credits;R0 -> R1;R1 credits;R1 -> R2;R2 credits;R2 -> node"
print(header)

time = [x for x in range(len(r2_1_cr))]

for index in range(len(time)):
    print("{};{};{}:{};{};{}:{};{};{}:{}".format(
            time[index],
            r0_credits[index],
            r0_1_lt[index],
            r1_0_cr[index],
            r1_credits[index],
            r1_2_lt[index],
            r2_1_cr[index],
            r2_credits[index],
            r2_node_lt[index],
            node_r2_cr[index],
        )
    )
