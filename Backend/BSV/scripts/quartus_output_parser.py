#!/bin/python3

# Author: Ivan Perez Gallardo
# Affiliation: University of Cantabria
# Date: 2019/05/21

# TODO: description of the script

import re
import sys
import os
import math


# Read fmax
def extract_resources(sim):
    filename = "{}/SMART_NEBB.fit.summary".format(sim)
    alms = "-1" #Adaptive Logic Modules
    registers  = "-1"
    memory = "-1"
    with open(filename, "r") as f:
        for line in f:
            if "Logic utilization" in line:
                alms = line.split(":")[1].split("/")[0].replace(",","").replace(" ","")
            
            if "Total registers" in line:
                registers = line.split(":")[1].split("/")[0].replace(",","").replace(" ","").replace("\n","")
            
            if "Total block memory bits" in line:
                memory = line.split(":")[1].split("/")[0].replace(",","").replace(" ","")
            

    result = [alms, registers, memory]
    return result

# Read fmax
def extract_frequency(sim):
    filename = "{}/SMART_NEBB.sta.rpt".format(sim)
    fmax_85 = "-1"
    fmax_0  = "-1"
    with open(filename, "r") as f:
        for line in f:
            if "; Slow 1100mV 85C Model Fmax Summary" in line:
                f.readline()
                f.readline()
                f.readline()
                rline = f.readline()
                fmax_85 = rline.split(";")[1].replace("MHz", "").replace(" ","")
            
            if "; Slow 1100mV 0C Model Fmax Summary" in line:
                f.readline()
                f.readline()
                f.readline()
                rline = f.readline()
                fmax_0 = rline.split(";")[1].replace("MHz", "").replace(" ","")

    result = [fmax_85, fmax_0]
    return result

# Read power
def extract_powers(sim):
    filename = "{}/SMART_NEBB.pow.rpt".format(sim)
    dp_network = 0.0  #total dynamic power 
    dp_input_units = 0.0  #total input units 
    dp_sa_l = 0.0
    dp_cb = 0.0
    dp_crd_units = 0.0
    dp_vca = 0.0
    with open(filename, "r") as f:
        for line in f:
            fields = line.split(";")
            if len(fields) < 2:
                continue

            if "|mkNetwork:meshNtk" in fields[1]:
                dp_network += float(fields[2].split(" mW")[0])
            
            if "|mkInputUnit:inputUnits_" in fields[1]:
                dp_input_units += float(fields[2].split(" mW")[0])
            
            if "|mkSwitchAllocUnit:localSAUnit" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            if "|FIFOL1:pipe_sal_sag_flit_" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            if "|FIFOL1:pipe_headers_" in fields[1]:
                dp_sa_l += float(fields[2].split(" mW")[0])
            
            if "|mkCrossbarSwitch:cbSwitch" in fields[1]:
                dp_cb += float(fields[2].split(" mW")[0])
            if "|FIFOL1:sa2cb_" in fields[1]:
                dp_cb += float(fields[2].split(" mW")[0])
            
            if "|mkReverseCreditUnit:crdUnits_" in fields[1]:
                dp_crd_units += float(fields[2].split(" mW")[0])
            
            if "|mkSmartVCAllocUnit:vcAllocUnits_" in fields[1]:
                dp_vca += float(fields[2].split(" mW")[0])
            

    result = [dp_network, dp_input_units, dp_sa_l, dp_cb, dp_crd_units,dp_vca]
    return result





sim_dir = sys.argv[1]
nodes = int(sys.argv[2])
k = math.sqrt(nodes)

header = [
                "config",
                "alms",
                "registers",
                "memory",
                "fmax_slow_85c_mhz",
                "fmax_slow_0c_mhz",
                "dp_network", #total dynamic power 
                "dp_input_units", #total input units 
                "dp_sa-l",
                "dp_cb",
                "dp_crd_units",
                "dp_vca"
		]

# XXX: I look for the beginning of the block with stats.
header_str = ','.join(header)
print(header_str)

simulations = os.listdir(sim_dir)

for sim in simulations:
    sim_values = [sim]
    
    temp = extract_resources(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(val)

    temp = extract_frequency(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(val)

    temp = extract_powers(sim_dir + '/' + sim)
    for val in temp:
        sim_values.append(str(val))

    sim_str = ','.join(sim_values)
    print(sim_str)

