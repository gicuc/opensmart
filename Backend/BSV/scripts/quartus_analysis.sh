#!/bin/bash


BUILDDIR=./build
CXXFLAGS="-Wall -Wno-unused -O0 -g -D_FILE_OFFSET_BITS=64 -j 3 -L/usr/lib/x86_64-linux-gnu"
INCLUDES="lib:src:src/Types:testbenches"
TYPES_TEMPLATE=scripts/templates/Types_template_quartus.bsv
TESTBENCH_TEMPLATE_SYN=scripts/template/mkTestBench.v.4x4_synthesis
TESTBENCH_TEMPLATE_SIM=scripts/template/mkTestBench.v.4x4_sim
BL_MAIN=/home/ivan/ATC/Bluespec-2017.07.A/lib/Verilog/main.v
BL_CONSTRAND=/home/ivan/ATC/Bluespec-2017.07.A/lib/Verilog/ConstrainedRandom.v
DATE=`date +"%Y_%m_%d"`
OUTDIR=output_quartus/${DATE}
QPROJECT_DIR=/home/ivan/intelFPGA_lite/18.1/projects/SMART_NEBB

function gen_SMART_verilog {
	mkdir -p $BUILDDIR
	mkdir -p $BUILDDIR/bdir
	bsc -verilog -g mkTestBench -D SMART -aggressive-conditions -no-warn-action-shadowing -simdir $BUILDDIR/bdir -info-dir $BUILDDIR/bdir -bdir $BUILDDIR/bdir -p +:$INCLUDES -u ./testbenches/TestBench.bsv 
	mkdir -p Verilog
	mv src/*.v ./Verilog/
	mv src/Types/*.v ./Verilog/
	mv testbenches/*.v ./Verilog/
}

function sim_SMART_verilog {
	bsc --verilog -e mkTestBench --vsim modelsim *.v
	./a.out
}

function run_quartus {
	quartus_map SMART_NEBB -c SMART_NEBB
	quartus_fit SMART_NEBB -c SMART_NEBB
	quartus_sta SMART_NEBB -c SMART_NEBB
	quartus_pow SMART_NEBB -c SMART_NEBB
}

############################## MAIN ###############################

mkdir $OUTDIR;

IR=20

#for vcs in {1,2,4,8};
for vcs in {1};
do 
	#for bufsize in {1,2,4,8};
	for bufsize in {8};
	do
		new_name=mesh_4x4_vcs_${vcs}_buf_${bufsize}

		echo "Processing... IR = ${IR} | VCs = ${vcs} | Buf. Size = ${bufsize}"

		# Generate new Types.bsv file
		cp ${TYPES_TEMPLATE} src/Types/Types.bsv;
		sed -i "s/typedef XX InjectionRate;/typedef ${IR} InjectionRate;/g" src/Types/Types.bsv
		sed -i "s/typedef XX NumUserVCs;/typedef ${vcs} NumUserVCs;/g" src/Types/Types.bsv
		sed -i "s/typedef XX MaxVCDepth;/typedef ${bufsize} MaxVCDepth;/g" src/Types/Types.bsv

		# Generate Verilog files from BSV
		echo "Generating Verilog file..."
		gen_SMART_verilog

		# Replace mkTestBench.v file for modelsim simulation with dumpvars
		echo "Replacing mkTestBench for simulations..."
		cp $TESTBENCH_TEMPLATE_SIM Verilog/mkTestBench.v

		# Replace main.v and ConstrainedRandom.v for simulation
		echo "Replacing main.v and ConstrainedRandom.v for simulations..."
		cp $BL_MAIN.sim $BL_MAIN
		cp $BL_CONSTRAND.sim $BL_CONSTRAND
		# Perform modelsim simulation
		echo "Running modelsim simulation..."
		cd Verilog
		sim_SMART_verilog
		cp dump.vcd $QPROJECT_DIR/
		du -h dump.vcd
		cd ..
		echo "VCD copied... Current dir:"
		pwd


		# Copy Verilog dir to quartus project
		echo "Copying Verilog files to quartus project..."
		cp -r Verilog $QPROJECT_DIR/
		# Replace mkTestBench for synthesis
		echo "Replacing mkTestBench.v, main.v and ConstrainedRandom.v for synthesis..."
		cp $TESTBENCH_TEMPLATE_SYN $QPROJECT_DIR/Verilog/mkTestBench.v
		# Replace main.v and ConstrainedRandom.v for synthesis
		cp $BL_MAIN.synthesis $BL_MAIN
		cp $BL_CONSTRAND.synthesis $BL_CONSTRAND
		# Run Quartus
		echo "Running Quartus..."
		cd $QPROJECT_DIR
		run_quartus
		cd -
		# Copy output files
		echo "Copying output files to $OUTDIR/${new_name}..."
		cp -r $QPROJECT_DIR/output_files $OUTDIR/${new_name}_output_files
	done
done


