#!/bin/bash

HOMEDIR=/home/ivan/ATC/Tools/OpenSMART/Backend/BSV
BUILDDIR=$HOMEDIR/build
CXXFLAGS="-Wall -Wno-unused -O0 -g -D_FILE_OFFSET_BITS=64 -j 3 -L/usr/lib/x86_64-linux-gnu"
INCLUDES="lib:src:src/Types:testbenches"
TYPES_TEMPLATE=scripts/templates/Types_template_quartus.bsv
TESTBENCH_TEMPLATE_SYN=scripts/templates/mkRouterTestBench.v.synthesis
TESTBENCH_TEMPLATE_SIM=scripts/templates/mkRouterTestBench.v.sim
BL_MAIN=/home/ivan/ATC/Bluespec-2017.07.A/lib/Verilog/main.v
BL_CONSTRAND=/home/ivan/ATC/Bluespec-2017.07.A/lib/Verilog/ConstrainedRandom.v
DATE=`date +"%Y_%m_%d"`
OUTDIR=output_quartus/router/${DATE}
QPROJECT_DIR=/home/ivan/intelFPGA_lite/18.1/projects/RouterTestBench

function gen_SMART_verilog {
	mkdir -p $BUILDDIR
	mkdir -p $BUILDDIR/bdir
	bsc -verilog -g mkRouterTestBench -D SMART  -show-schedule -aggressive-conditions -no-warn-action-shadowing +RTS -K128M -RTS -simdir $BUILDDIR/bdir -info-dir $BUILDDIR/bdir -bdir $BUILDDIR/bdir -p +:$INCLUDES -u ./testbenches/RouterTestBench.bsv
    mkdir -p RouterVerilog
    mv src/*.v ./RouterVerilog/
    mv src/Types/*.v ./RouterVerilog/
    mv testbenches/*.v ./RouterVerilog/
}

function sim_SMART_verilog {
	bsc --verilog -e mkTestBench --vsim modelsim *.v
	./a.out
}

function run_quartus {
	quartus_map RouterTestBench -c RouterTestBench
	quartus_fit RouterTestBench -c RouterTestBench
	quartus_asm RouterTestBench -c RouterTestBench
	quartus_eda --write_settings_files=off --simulation --functional=on --flatten_buses=off --tool=modelsim_oem --format=verilog --output_directory="/home/ivan/intelFPGA_lite/18.1/projects/RouterTestBench/simulation/qsim/" RouterTestBench -c RouterTestBench
	quartus_eda --gen_testbench --tool=modelsim_oem --format=verilog --write_settings_files=off RouterTestBench -c RouterTestBench --vector_source="/home/ivan/intelFPGA_lite/18.1/projects/RouterTestBench/Waveform.vwf" --testbench_file="/home/ivan/intelFPGA_lite/18.1/projects/RouterTestBench/simulation/qsim/Waveform.vwf.vt"
	cd simulation/qsim
	/home/ivan/intelFPGA_lite/18.1/modelsim_ase/linuxaloem//vsim -c -do RouterTestBench.do
	cd ../..
	quartus_sta RouterTestBench -c RouterTestBench
	quartus_pow RouterTestBench -c RouterTestBench
}

############################## MAIN ###############################

mkdir -p $OUTDIR;

IR=20

for vcs in {1,2,4,8};
do 
	for bufsize in {1,2,4,5,8,10,15,20};
	do
		new_name=router_vcs_${vcs}_buf_${bufsize}

		echo "Processing... IR = ${IR} | VCs = ${vcs} | Buf. Size = ${bufsize}"

		# Generate new Types.bsv file
		cp ${TYPES_TEMPLATE} src/Types/Types.bsv;
		sed -i "s/typedef XX InjectionRate;/typedef ${IR} InjectionRate;/g" src/Types/Types.bsv
		sed -i "s/typedef XX NumUserVCs;/typedef ${vcs} NumUserVCs;/g" src/Types/Types.bsv
		sed -i "s/typedef XX MaxVCDepth;/typedef ${bufsize} MaxVCDepth;/g" src/Types/Types.bsv

		# Generate Verilog files from BSV
		echo "Generating Verilog file..."
		gen_SMART_verilog

		## Replace mkTestBench.v file for modelsim simulation with dumpvars
		#echo "Replacing mkTestBench for simulations..."
		#cp $TESTBENCH_TEMPLATE_SIM Verilog/mkTestBench.v

		## Replace main.v and ConstrainedRandom.v for simulation
		#echo "Replacing main.v and ConstrainedRandom.v for simulations..."
		#cp $BL_MAIN.sim $BL_MAIN
		#cp $BL_CONSTRAND.sim $BL_CONSTRAND
		## Perform modelsim simulation
		#echo "Running modelsim simulation..."
		#cd Verilog
		#sim_SMART_verilog
		#cp dump.vcd $QPROJECT_DIR/
		#du -h dump.vcd
		#cd ..
		#echo "VCD copied... Current dir:"
		#pwd


		# Copy Verilog dir to quartus project
		echo "Copying Verilog files to quartus project..."
		cp -r RouterVerilog $QPROJECT_DIR/
		# Replace mkTestBench for synthesis
		echo "Replacing mkTestBench.v, main.v for synthesis..."
		#cp $TESTBENCH_TEMPLATE_SYN $QPROJECT_DIR/RouterVerilog/mkTestBench.v
		# Replace main.v
		cp $BL_MAIN.synthesis $BL_MAIN
		# Run Quartus
		echo "Running Quartus..."
		cd $QPROJECT_DIR
		# Add "test" output port and assign some signals to it
		sed -i 's/RST_N);/RST_N, test);/g' RouterVerilog/mkRouterTestBench.v
		sed -i 's/  input  RST_N;/input  RST_N;\n  output test;/g' RouterVerilog/mkRouterTestBench.v
		sed -i 's/  always@(next_port_0/  assign test = router$RDY_dataLinks_0_getFlit \& router$RDY_dataLinks_1_getFlit \& router$RDY_dataLinks_2_getFlit \& router$RDY_dataLinks_3_getFlit \& router$RDY_dataLinks_4_getFlit ;\n  always@(next_port_0/g' RouterVerilog/mkRouterTestBench.v
		run_quartus
		cd $HOMEDIR
		# Copy output files
		echo "Copying output files to $OUTDIR/${new_name}..."
		cp -r $QPROJECT_DIR/output_files $OUTDIR/${new_name}_output_files
	done
done


