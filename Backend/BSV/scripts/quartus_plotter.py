#!/bin/python3

# Author: Ivan Perez Gallardo
# Affiliation: University of Cantabria
# Date: 2019/05/21

import numpy as np
import matplotlib.pyplot as plt
import csv
import sys


def prettyConfigNames(config_names):
    new_cfg_names = []
    for cfg in config_names:
        new_cfg_names.append(cfg.replace("buf","x").replace("mesh_4x4","").replace("output_files","").replace("_"," ").replace("vcs","").replace("router ", "").replace(" ",""))
    return new_cfg_names

def fmax_85c_plot(filename):
    with open(filename) as f:
        reader = csv.DictReader(f, delimiter=',')
        fmax_slow_85c_mhz = [float(row["fmax_slow_85c_mhz"]) for row in reader]
        config = [row["config"] for row in reader]
        ind = np.arange(len(fmax_slow_0c_mhz))    # the x locations for the groups
        width = 0.35       # the width of the bars: can also be len(x) sequence
    
        p1 = plt.bar(ind, fmax_slow_0c_mhz, width)
    
    
    
        #plt.ylabel('Scores')
        #plt.title('Scores by group and gender')
        plt.set_xticklabels(config)
        #plt.yticks(np.arange(0, 81, 10))
        #plt.legend((p1[0], p2[0]), ('Men', 'Women'))
    
        plt.show()


def stacked_bar_plot(config_names, values, legend, ylabel, output_file):
    print(config_names)
    print(values)
    print(legend)

    figsize = (7,4)
        
    colormap = 'viridis'
    cmap = plt.get_cmap(colormap)
    total_colors = len(cmap.colors)
    # Find latest colormap index
    no_colors = len(legend)
    color_offset = int(total_colors/(no_colors))
    colors = list(reversed(cmap.colors))[0:-1:color_offset]

    fig, ax = plt.subplots(figsize=figsize)
            
    bottom = [0] * len(config_names)
    ind = np.arange(len(config_names))    # the x locations for the groups
    for index, value_series in enumerate(values):
        ax.bar(ind, value_series, width=0.75, bottom = bottom,
                label=legend[index],
                color=colors[index],
                edgecolor='black', linewidth=0.3)
        bottom = [x+y for x, y in zip(value_series, bottom)]

    ax.legend(loc=0)
    ax.set_ylabel(ylabel)
    ax.set_xlabel("# VCs x Buffer Size")
    ax.set_xticks(ind)

    ax.set_xticklabels(config_names, rotation='vertical')
    
    ax.grid(b=True, which='major',color='silver', linewidth=0.2, linestyle="-")

    #plt.show()
    fig.savefig(output_file, bbox_inches='tight',pad_inches=0)
    
######################### MAIN ###########################

filename = sys.argv[1]

header = [
                "config",
                "alms",
                "registers",
                "memory",
                "fmax_slow_85c_mhz",
                "fmax_slow_0c_mhz",
                "dp_network", #total dynamic power 
                "dp_input_units", #total input units 
                "dp_sa-l",
                "dp_cb",
                "dp_crd_units",
                "dp_vca"
		]

config_names = []
fmax_85 = []
legend = ["85 $^\circ$C"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        fmax_85.append(float(row["fmax_slow_85c_mhz"]))

values = [fmax_85]
config_names = prettyConfigNames(config_names)
ylabel = "Frequency (MHz)"
output_file = filename + "_freq.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)

config_names = []
dp_network = []
dp_input_units = []
dp_sa_l = []
dp_cb = []
dp_crd_units = []
dp_vca  = []
legend = ["Input units","SA-L","XBar","Credit units","VA","Misc"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        dp_network.append(float(row["dp_network"]))
        dp_input_units.append(float(row["dp_input_units"]))
        dp_sa_l.append(float(row["dp_sa-l"]))
        dp_cb.append(float(row["dp_cb"]))
        dp_crd_units.append(float(row["dp_crd_units"]))
        dp_vca.append(float(row["dp_vca"]))

dp_others = []
for index, x in enumerate(dp_network):
    dp_others.append(x - dp_input_units[index] - dp_sa_l[index] - dp_cb[index] - dp_crd_units[index] - dp_vca[index])

config_names = prettyConfigNames(config_names)
values = [dp_input_units, dp_sa_l, dp_cb, dp_crd_units, dp_vca, dp_others]
ylabel = "Dynamic Power (mW)"
output_file = filename + "_pow.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)

config_names = []
alms_network = []
alms_input_units = []
alms_sa_l = []
alms_cb = []
alms_crd_units = []
alms_vca  = []
legend = ["Input units","SA-L","XBar","Credit units","VA","Misc"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        alms_network.append(float(row["alms_network"]))
        alms_input_units.append(float(row["alms_input_units"]))
        alms_sa_l.append(float(row["alms_sa-l"]))
        alms_cb.append(float(row["alms_cb"]))
        alms_crd_units.append(float(row["alms_crd_units"]))
        alms_vca.append(float(row["alms_vca"]))

alms_others = []
for index, x in enumerate(alms_network):
    alms_others.append(x - alms_input_units[index] - alms_sa_l[index] - alms_cb[index] - alms_crd_units[index] - alms_vca[index])

config_names = prettyConfigNames(config_names)
values = [alms_input_units, alms_sa_l, alms_cb, alms_crd_units, alms_vca, alms_others]
ylabel = "# ALMs"
output_file = filename + "_alms.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)

config_names = []
alus_network = []
alus_input_units = []
alus_sa_l = []
alus_cb = []
alus_crd_units = []
alus_vca  = []
legend = ["Input units","SA-L","XBar","Credit units","VA","Misc"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        alus_network.append(float(row["alus_network"]))
        alus_input_units.append(float(row["alus_input_units"]))
        alus_sa_l.append(float(row["alus_sa-l"]))
        alus_cb.append(float(row["alus_cb"]))
        alus_crd_units.append(float(row["alus_crd_units"]))
        alus_vca.append(float(row["alus_vca"]))

alus_others = []
for index, x in enumerate(alus_network):
    alus_others.append(x - alus_input_units[index] - alus_sa_l[index] - alus_cb[index] - alus_crd_units[index] - alus_vca[index])

config_names = prettyConfigNames(config_names)
values = [alus_input_units, alus_sa_l, alus_cb, alus_crd_units, alus_vca, alus_others]
ylabel = "# ALUs"
output_file = filename + "_alus.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)

config_names = []
reg_network = []
reg_input_units = []
reg_sa_l = []
reg_cb = []
reg_crd_units = []
reg_vca  = []
legend = ["Input units","SA-L","XBar","Credit units","VA","Misc"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        reg_network.append(float(row["reg_network"]))
        reg_input_units.append(float(row["reg_input_units"]))
        reg_sa_l.append(float(row["reg_sa-l"]))
        reg_cb.append(float(row["reg_cb"]))
        reg_crd_units.append(float(row["reg_crd_units"]))
        reg_vca.append(float(row["reg_vca"]))

reg_others = []
for index, x in enumerate(reg_network):
    reg_others.append(x - reg_input_units[index] - reg_sa_l[index] - reg_cb[index] - reg_crd_units[index] - reg_vca[index])

config_names = prettyConfigNames(config_names)
values = [reg_input_units, reg_sa_l, reg_cb, reg_crd_units, reg_vca, reg_others]
ylabel = "# Dedicated Register"
output_file = filename + "_regs.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)

config_names = []
mem_network = []
mem_input_units = []
mem_sa_l = []
mem_cb = []
mem_crd_units = []
mem_vca  = []
legend = ["Input units","SA-L","XBar","Credit units","VA","Misc"]
with open(filename) as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        config_names.append(row["config"])
        mem_network.append(float(row["mem_network"]))
        mem_input_units.append(float(row["mem_input_units"]))
        mem_sa_l.append(float(row["mem_sa-l"]))
        mem_cb.append(float(row["mem_cb"]))
        mem_crd_units.append(float(row["mem_crd_units"]))
        mem_vca.append(float(row["mem_vca"]))

mem_others = []
for index, x in enumerate(mem_network):
    mem_others.append(x - mem_input_units[index] - mem_sa_l[index] - mem_cb[index] - mem_crd_units[index] - mem_vca[index])

config_names = prettyConfigNames(config_names)
values = [mem_input_units, mem_sa_l, mem_cb, mem_crd_units, mem_vca, mem_others]
ylabel = "Block Memory Bits"
output_file = filename + "_mem.pdf"
stacked_bar_plot(config_names, values, legend, ylabel, output_file)
