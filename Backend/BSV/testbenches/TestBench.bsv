import Vector::*;
import Connectable::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import Network::*;
import TrafficGeneratorUnit::*;
import TrafficGeneratorBuffer::*;
import CreditUnit::*;
import StatLogger::*;

typedef 99 InjectionRateStep;
//typedef 0001 InjectionRateInit; 
typedef 99 InjectionRateInit; 
//typedef 50 InjectionRateInit; 
typedef 01 InjectionRateMax; 

//typedef 5000 DrainCycles;
typedef 0 DrainCycles;


(* synthesize *)
//(* descending_urgency = "finishBench, rs"*)
module mkTestBench();

	/********************************* States *************************************/
	Reg#(Data) clkCount  <- mkReg(0);
	Reg#(Bool) inited    <- mkReg(False);
	Reg#(Data) initCount <- mkReg(0);

	// Injection Rate sweep
	//Reg#(Data) injection_rate <- mkReg(fromInteger(valueOf(InjectionRateInit)));
	Reg#(Data) injection_rate <- mkReg(fromInteger(valueOf(InjectionRate)));


	Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorUnit))    trafficGeneratorUnits       <- replicateM(replicateM(mkTrafficGeneratorUnit));
	//Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorBuffer))  trafficGeneratorBufferUnits <- replicateM(replicateM(mkTrafficGeneratorBuffer));

	Vector#(MeshHeight, Vector#(MeshWidth, ReverseCreditUnit))  creditUnits    <- replicateM(replicateM(mkReverseCreditUnit));
	Vector#(MeshHeight, Vector#(MeshWidth, StatLogger))         statLoggers    <- replicateM(replicateM(mkStatLogger));

	/******************************** Submodule ************************************/

	Network meshNtk <- mkNetwork;

	rule init(!inited);
		if(initCount == 0)
		begin
			clkCount <= 0;
			for (Integer i=0; i<valueOf(MeshHeight); i=i+1)
			begin
				for (Integer j=0; j<valueOf(MeshWidth); j=j+1)
				begin
					trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j));
				end
			end
		end

		initCount <= initCount + 1;
		if (meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))
		begin
			inited <= True;
		end 
	endrule

	rule doClkCount(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)+valueOf(DrainCycles)));
		`ifdef DEBUG
			if(clkCount % 10000 == 0) begin
				$display("Elapsed Simulation Cycles: %d",clkCount);
			end
		`endif

		clkCount <= clkCount + 1;
	endrule

	Rules rs =  //XXX: This is for descending_urgency attribute
	rules
		rule finishBench(inited && clkCount == fromInteger(valueOf(BenchmarkCycle)+valueOf(DrainCycles)));
			Data res = 0;
			Data send = 0;
			Data hop = 0;
			Data smart_hops = 0;
			Data remainingFlits = 0;
			Data inflight = 0;
			Bit#(64) resLatency = 0;

			for (Integer i=0; i<valueOf(MeshHeight); i=i+1)
			begin
				for(Integer j=0; j<valueOf(MeshWidth); j=j+1)
				begin
					Data sendCount = statLoggers[i][j].getSendCount;
					Data recvCount = statLoggers[i][j].getRecvCount;
					Data latencyCount = statLoggers[i][j].getLatencyCount;
					//Data remFlits = trafficGeneratorBufferUnits[i][j].getRemainingFlitsNumber;
                    // FIXME: trafficGeneratorUnits[i][j].getRemainingFlitsNumber;
					Data remFlits = 0;
					//          Data extraLatency = trafficGeneratorBufferUnits[i][j].getExtraLatency(clkCount);
					Data hopCount = statLoggers[i][j].getHopCount;
					Data smartHops = statLoggers[i][j].getSmartHops;
					Data inflightCycle = statLoggers[i][j].getInflightLatencyCount;

					$display("send_count[%d][%d] = %d, recv_count[%d][%d] = %d", i, j, sendCount, i, j, recvCount);

					send = send + sendCount;
					res = res + recvCount;
					resLatency = resLatency + zeroExtend(latencyCount);// + zeroExtend(extraLatency);
					remainingFlits = remainingFlits + remFlits;
					hop = hop + hopCount;
					smart_hops = smart_hops + smartHops;
					inflight = inflight + inflightCycle;
				end
			end

				$display("Elapsed clock cycles: %d", clkCount);
				$display("Injection rate: %d",injection_rate); //XXX: Nuevo campo para mostrar la tasa de inyeccion fijada
				$display("Total injected packet: %d",send);
				$display("Total received packet: %d",res);
				$display("Total latency: %d", resLatency);
				$display("Total hopCount: %d", hop);
				$display("Total SMART hops: %d", smart_hops);
				$display("Total inflight latency: %d", inflight);
				$display("Number of remaiing Flits in traffic generator side: %d", remainingFlits);
			

			//$finish; // XXX: reemplazado por control del injection Rate
			//
			let step_ir = fromInteger(valueOf(InjectionRateStep));
			let next_injection_rate = injection_rate < step_ir ? step_ir : injection_rate + step_ir;
			if (next_injection_rate > fromInteger(valueOf(InjectionRateMax)))
			begin
				$finish;
			end
			else begin
				injection_rate <= next_injection_rate;
				clkCount <= 0;
				for(Integer i=0; i<valueOf(MeshHeight); i=i+1)
				begin
					for(Integer j=0; j<valueOf(MeshWidth); j=j+1)
					begin
						trafficGeneratorUnits[i][j].setInjectionRate(next_injection_rate);
						statLoggers[i][j].resetCount;
					end
				end
			end
		endrule
	endrules;


	//Credit Links
	for(Integer i=0; i<valueOf(MeshHeight); i=i+1)
	begin
		for(Integer j=0; j<valueOf(MeshWidth); j=j+1)
		begin

			mkConnection(creditUnits[i][j].getCredit,
			meshNtk.ntkPorts[i][j].putCredit);

			mkConnection(meshNtk.ntkPorts[i][j].getCredit,
			trafficGeneratorUnits[i][j].putVC);
		end
	end


	//Data Links
	for(Integer i=0; i<valueOf(MeshHeight); i=i+1)
	begin
		for(Integer j=0; j<valueOf(MeshWidth); j=j+1)
		begin

			Rules r = // XXX: descending_urgency
			rules

				rule genFlits(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)));
					// FIXME: estoy forzado a que solo el nodo 0 inyecte
					//if (i == 0 && (j == 0 || j == 1)) begin
					//if (i == 0 && j == 0) begin
					//if (i == 0 && (j == 0 || j == 5)) begin
					trafficGeneratorUnits[i][j].genFlit(clkCount);
					//end
				endrule

				//rule prepareFlits(inited);
				//	let flit <- trafficGeneratorUnits[i][j].getFlit;
				//	trafficGeneratorBufferUnits[i][j].putFlit(flit);//, clkCount);
				//endrule

				rule putFlits(inited);
					//let flit <- trafficGeneratorBufferUnits[i][j].getFlit;
					let flit <- trafficGeneratorUnits[i][j].getFlit;
					
					`ifdef DEBUG
						flit.stat.inflightCycle = clkCount;
					`endif

					meshNtk.ntkPorts[i][j].putFlit(flit);
					statLoggers[i][j].incSendCount;
					`ifdef DEBUG
						$display("TestBench: %m - Flit %d - Generated from(%d, %d). The destination is (%d, %d)  source: (%d, %d)", flit.stat.flitId, fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
					`endif
				endrule

				rule getFlits(inited);
					let flit <- meshNtk.ntkPorts[i][j].getFlit;
					`ifdef DEBUG
						Data hopCount = flit.stat.hopCount;
						Data smartHops = flit.stat.smartHops;
					`else
						Data hopCount = 0;
						Data smartHops = 0;
					`endif
					`ifdef DEBUG
						$display("%t | %m | Flit reached destination | Flit %d | Source: %d_%d | Destination: %d_%d", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX);
					`endif

					`ifdef DEBUG
						if((flit.stat.dstX != fromInteger(j)) || (flit.stat.dstY != fromInteger(i))) begin
							$display("Warning: Missrouted - Flit %d - Received from (%d, %d) but the destination is (%d, %d) source: (%d, %d)", flit.stat.flitId, fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
						end

						else begin
							$display("Correct - Flit %d - Received from(%d, %d). The destination is (%d, %d)  source: (%d, %d)", flit.stat.flitId, fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
						end
					`endif
					`ifdef DEBUG 
						//let tot_lat = clkCount >= flit.stat.injectedCycle ? clkCount - flit.stat.injectedCycle : clkCount + fromInteger(valueOf(BenchmarkCycle)) - flit.stat.injectedCycle;
						let tot_lat = clkCount >= flit.stat.injectedCycle ? clkCount - flit.stat.injectedCycle : 0;
						if (clkCount <= flit.stat.injectedCycle) begin
							//$display("Warning: flit injection cycle lower that current clk count. clkCount: %d - injectedCycle: %d", clkCount, flit.stat.injectedCycle);
						end
						//let inf_lat = clkCount >= flit.stat.inflightCycle ? clkCount - flit.stat.inflightCycle : clkCount + fromInteger(valueOf(BenchmarkCycle)) - flit.stat.inflightCycle;
						let inf_lat = clkCount >= flit.stat.inflightCycle ? clkCount - flit.stat.inflightCycle : 0;
					`else
						let tot_lat = 0;
						let inf_lat = 0;
					`endif

					statLoggers[i][j].incLatencyCount(tot_lat);
					statLoggers[i][j].incInflightLatencyCount(inf_lat);
					statLoggers[i][j].incRecvCount;
					statLoggers[i][j].incHopCount(hopCount);
					statLoggers[i][j].incSmartHops(smartHops);

					`ifdef DEBUG 
						creditUnits[i][j].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, flitId: flit.stat.flitId}));
					`else
						// FIXME: Remove flitId if DEBUG flags are disabled.
						creditUnits[i][j].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True}));
					`endif

				endrule

			endrules;
			rs = rJoinDescendingUrgency(rs, r);

		end
	end
	addRules(rs);

endmodule
