import Types::*;
import Vector::*;
//import Router::*;
import Fifo::*;
import SmartRouterTypes::*;
import SmartRouter::*;
import SmartVCAllocUnit::*;
import VCAllocUnitTypes::*;
import VirtualChannelTypes::*;
import SSR_Manager::*;
import MessageTypes::*;
import SmartTypes::*;
import CreditTypes::*;
import RoutingTypes::*;
import Connectable::*;
//import BaselineRouter::*;

/*
  Just for Area/Power estimation of a single router
*/

//interface RouterTestBench;
//  method Action startRun;
//  method Bool isFinished;
//  method ActionValue#(Data) getFLitCount;
//endinterface

//module mkRouterTestBench(RouterTestBench);
module mkRouterTestBench();
  Router router <- mkSmartRouter;
  Vector#(NumPorts, Vector#(NumVCs, Reg#(int))) credits <- replicateM(replicateM(mkReg(fromInteger(valueOf(MaxVCDepth)))));
  Vector#(NumPorts, Vector#(NumVCs, PulseWire)) inc_credits <- replicateM(replicateM(mkPulseWire));
  Vector#(NumPorts, Vector#(NumVCs, PulseWire)) dec_credits <- replicateM(replicateM(mkPulseWire));
  Vector#(NumPorts, Wire#(VCIdx)) next_vc <- replicateM(mkWire);
  Vector#(NumPorts, Reg#(Data)) next_port <- replicateM(mkReg(0));
  Reg#(Bool) started <- mkReg(False);
  Reg#(Data) clkCount <- mkReg(0);
  Vector#(NumPorts, Reg#(Data)) flitCount <- replicateM(mkReg(0));

  rule init(!started);
	if (router.isInited()) begin
	    started <= True;
		clkCount <= 0;
		//flitCount <= 0;
		$dumpvars;
	end
  endrule
 
  rule doCount(started);
    if(clkCount == 1000) begin
      started <= False;
      clkCount <= 0;
	  $finish;
    end
    else begin
	  $display("clkCount: %d", clkCount);
      clkCount <= clkCount +1;
    end

  endrule

  for(Integer i=0;i<valueOf(NumPorts); i=i+1) begin
	rule updateNextPort(started);
        let incr = case(i)
                 0: 7;
				 1: 13;
				 2: 17;
				 3: 29;
				 4: 31;
			       endcase;
		next_port[i] <= next_port[i] + incr;
		$display("updateNextPort: %d", next_port[i]);
	endrule

    rule insertFlits(started);
	  int vc_hasCredit = -1;
	  for (VCIdx vc= 0; vc < fromInteger(valueOf(NumVCs)); vc = vc +1) begin
		if (credits[i][vc] > 0) begin
			vc_hasCredit = unpack(zeroExtend(vc));
		end
	  end
	  if(vc_hasCredit > -1) begin
      	Flit flit = ?;
     	flit.vc = pack(truncate(vc_hasCredit));
		`ifdef DEBUG
	  		flit.stat.flitId = clkCount;
		`endif
	  	flit.flitType = HeadTail;
		Bit#(4) x = truncate(next_port[i]);
        flit.routeInfo.nextDir = case(x)
                 0: east_;
				 1: south_;
				 2: west_;
				 3: north_;
				 default: local_;
			       endcase;
		`ifdef DEBUG
	    $display("insertFlits: %d", flit.stat.flitId);
		`endif
        flit.routeInfo.numXhops = truncate(clkCount);
        flit.routeInfo.numYhops = truncate(clkCount);

        router.dataLinks[i].putFlit(flit);
		dec_credits[i][vc_hasCredit].send();
  	  end
    endrule

    rule getFlits(started);
      let flit <- router.dataLinks[i].getFlit;
	  `ifdef DEBUG
	  $display("Receive Flit: %d", flit.stat.flitId);
  	  `endif
      flitCount[i] <= flitCount[i] + 1;
      router.controlLinks[i].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True}));
    endrule

    //rule putCredits(started);
    //endrule

  end
	
  //Credit Links
  for(Integer i=0; i<valueOf(NumPorts); i=i+1)
  begin
	rule recCredits(started);
  		CreditSignal c <- router.controlLinks[i].getCredit;
		if(isValid(c)) begin
  			//vcAllocUnit[i].putFreeVC(validValue(c).vc);
			inc_credits[i][validValue(c).vc].send();
		end
	endrule

	for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc +1) begin
		rule updateCredits(started);
			int x = 0;
			if (inc_credits[i][vc]) begin
				x = x + 1;
			end

			if (dec_credits[i][vc]) begin
				x = x - 1;
			end

			credits[i][vc] <= credits[i][vc] + x;
		endrule
	end
  end



  //method ActionValue#(Data) getFLitCount;
  //  return flitCount;
  //endmethod

  //method Action startRun;
  //  started <= True;
  //endmethod

  //method Bool isFinished = !started;

endmodule
