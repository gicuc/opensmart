import Vector::*;
//import Fifo::*;
import FIFOF::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import TrafficGenerator::*;
import SmartVCAllocUnit::*;

interface TrafficGeneratorUnit;
  method Action initialize(MeshHIdx yID, MeshWIdx xID);
  method Action genFlit(Data clkCount);
  method ActionValue#(Flit) getFlit;
  method Action putVC(CreditSignal sig);
	method Action setInjectionRate(Data ir);
endinterface

(* synthesize *)
module mkTrafficGeneratorUnit(TrafficGeneratorUnit);
  SmartVCAllocUnit vcAllocUnit <- mkSmartVCAllocUnit;
  TrafficGenerator trafficGenerator <- mkUniformRandom;
//  TrafficGenerator trafficGenerator <- mkBitComplement;

  //Fifo#(NumTrafficGeneratorBufferSlots, Flit) tempFifo <- mkBypassFifo;
  FIFOF#(Flit) tempFifo <- mkSizedFIFOF(valueOf(NumTrafficGeneratorBufferSlots));
  //Fifo#(1, Flit) outFifo <- mkBypassFifo;
  Wire#(Flit) outFifo <- mkWire;

  // IVAN
  Reg#(Data) id <- mkReg(0);

/*
  rule rl_getFlit;
    let flit <- trafficGenerator.getFlit;
    tempFifo.enq(flit);
  endrule
*/

  //rule rl_getVC(vcAllocUnit.hasVC);
  rule rl_getVC;
    let flit = tempFifo.first;
    tempFifo.deq;
    let vc <- vcAllocUnit.getNextVC;
    flit.vc = vc;
    //outFifo.enq(flit);
    outFifo <= flit;
  endrule

  method Action genFlit(Data clkCount);
	  //if(vcAllocUnit.hasVC2) begin
      $display("%t | %m | genFlit | hasVC: %b | ", $time, vcAllocUnit.hasVC);

	  if(vcAllocUnit.hasVC) begin
		  let flit <- trafficGenerator.getFlit;

		  `ifdef DEBUG
			  //flit.stat.injectedCycle = clkCount;
			  // IVAN
			  flit.stat.flitId = id;
		  `endif
		  id <= id + 1;
		  tempFifo.enq(flit);
	  end
  endmethod

  method Action initialize(MeshHIdx yID, MeshWIdx xID);
    trafficGenerator.initialize(yID, xID);
  endmethod

  method ActionValue#(Flit) getFlit;
    //outFifo.deq;
    //return outFifo.first;
    return outFifo;
  endmethod

  method Action putVC(CreditSignal sig);
    if(isValid(sig)) begin
      vcAllocUnit.putFreeVC(validValue(sig).vc);
    end
  endmethod
	
	method Action setInjectionRate(Data ir);
		trafficGenerator.setInjectionRate(ir);
	endmethod

endmodule
