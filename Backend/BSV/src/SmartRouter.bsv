import Vector::*;
import Fifo::*;
import FIFO::*;
import FIFOF::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import SwitchAllocTypes::*;
import VCAllocUnitTypes::*;
import RoutingUnitTypes::*;
import SmartTypes::*;
import SmartRouterTypes::*;

import InputUnit::*;
//import OutputUnit::*;
import CreditUnit::*;
import SwitchAllocUnit::*;
import CrossbarSwitch::*;
import SmartVCAllocUnit::*;
import GlobalSwitchAllocUnit::*;
import SmartFlag::*;
import RoutingUnit::*;
import SSR_IO_Unit::*;

/* Get/Put Context 
 *  Think from outside of the module; 
 *  Ex) I am "getting" a flit from this module. 
 *      I am "putting" a flit toward this module.
 */


/*
typedef struct {
  SARes grantedInPorts;
  SARes grantedOutPorts;
  HeaderBundle hb;
  FlitBundle fb;
} SA2CB deriving (Bits, Eq);
*/


typedef struct {
  Header header; 
  Flit flit;
} SA2CB deriving (Bits, Eq);


//(* synthesize, always_ready, always_enabled *)
(* synthesize *)
module mkSmartRouter(Router);

  /********************************* States *************************************/
  Reg#(Bool)                                inited 		        <- mkReg(False);

  Vector#(NumPorts, Wire#(Flit))            smartFlitBuf  		<- replicateM(mkWire);

  Wire#(FlitBundle)                         flitsBuf      		<- mkWire;
  Wire#(HeaderBundle)                       headersBuf   		<- mkWire;
  Wire#(SARes)                              saResInBuf  		<- mkWire;

  //Pipelining
  // Stage 1 to 2
  Vector#(NumPorts, FIFOF#(Maybe#(Header)))	pipe_headers        <- replicateM(mkLFIFOF);
  Vector#(NumPorts, FIFOF#(Bool))     		pipe_grantedInputs  <- replicateM(mkLFIFOF);
  Vector#(NumPorts, FIFOF#(Bool))			pipe_grantedOutputs <- replicateM(mkLFIFOF);
  Wire#(SARes)								w_grantedInputs  	<- mkDWire(0);
  Wire#(SARes)								w_grantedOutputs 	<- mkDWire(0);
  Vector#(NumPorts, FIFOF#(Flit))			pipe_sal_sag_flit	<- replicateM(mkLFIFOF); 
  Vector#(NumPorts, FIFOF#(Flit))			pipe_sal_sag_header	<- replicateM(mkLFIFOF); 

  //Stage 2 to 3
  Vector#(NumPorts, FIFOF#(SA2CB))			sa2cb               <- replicateM(mkLFIFOF);
  Vector#(NumPorts, Wire#(Flit))			outputUnits			<- replicateM(mkWire);

  /******************************* Submodules ***********************************/

  /* Input Side */
  Vector#(NumPorts, InputUnit)          inputUnits   <- replicateM(mkInputUnit);
  Vector#(NumPorts, ReverseCreditUnit)  crdUnits     <- replicateM(mkReverseCreditUnit);
  GlobalSwitchAllocUnit                 globalSAUnit <- mkGlobalSwitchAllocUnit;

  /* In the middle */
  SwitchAllocUnit                       localSAUnit  <- mkSwitchAllocUnit;
  CrossbarSwitch                        cbSwitch     <- mkCrossbarSwitch;

  /* Output Side */
  Vector#(NumPorts, SmartVCAllocUnit)   vcAllocUnits <- replicateM(mkSmartVCAllocUnit);
  Vector#(NumPorts, RoutingUnit)        routingUnits <- replicateM(mkRoutingUnit); 
  Vector#(NumPorts, SSR_IO_Unit)        ssrIOUnits   <- replicateM(mkSSR_IO_Unit);
  Vector#(NumPorts, SmartFlagUnit)      smartFlags   <- replicateM(mkSmartFlagUnit);


  /******************************* Functions ***********************************/
  /* Read Inputs */
	function FlitBundle readFlits;
		FlitBundle currentFlits = newVector;

		for (Integer inPort=0; inPort<valueOf(NumPorts) ; inPort=inPort+1)
		begin
			currentFlits[inPort] = inputUnits[inPort].peekFlit;
		end

		return currentFlits;
	endfunction
  

	function HeaderBundle readHeaders;
    	HeaderBundle hb = newVector;

		for (Integer inPort = 0; inPort<valueOf(NumPorts); inPort = inPort +1)
		begin
			hb[inPort] =  inputUnits[inPort].peekHeader;
		end

		return hb;
	endfunction

	
	function Action putFlit2XBar(Flit flit, Header header, DirIdx inPort);
	action
		let destDirn = dir2Idx(header.routeInfo.nextDir);
		cbSwitch.crossbarPorts[inPort].putFlit(flit, destDirn);
	endaction
	endfunction

	
	function FreeVCInfo getFreeVCInfo;
		FreeVCInfo ret = ?;

		for (Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
			ret[outPort] = vcAllocUnits[outPort].hasVC? 1:0;
		end

		return ret;
	endfunction

	function FreeVCInfo getFreeVCInfo2;
		FreeVCInfo ret = ?;

		for (Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
			ret[outPort] = vcAllocUnits[outPort].hasVC2? 1:0;
		end

		return ret;
	endfunction


	// TODO: Put this directly in sendSSR.  
	function Action setupFlags(SARes grantedOutPorts, FreeVCInfo freeVCInfo);
	action  

		for (Integer outPort = 0; outPort < valueOf(NumNormalPorts); outPort = outPort+1)
		begin

			let hasSSR <- ssrIOUnits[outPort].hasInSSR_Requester;

			let newFlag = (grantedOutPorts[outPort] == 0    // 1. No local winner for the output port(outPort)
			&& hasSSR                       // 2. Has at least one incoming SSR
			&& freeVCInfo[outPort] == 1)?   // 3. Has a free VC at the next router
			//   -> Winner implies it has at least one free VC
			Pass:Stop;

			smartFlags[outPort].setFlag(newFlag);

			if (grantedOutPorts[outPort] == 1 || newFlag == Pass) begin
				vcAllocUnits[outPort].doingSAG();
			end
		end

		let localPort = valueOf(NumPorts) - 1;
		if (grantedOutPorts[localPort] == 1) begin
			vcAllocUnits[localPort].doingSAG();
		end

	endaction
	endfunction

	/****************************** Router Behavior ******************************/
	rule doInitialize(!inited);
		Bit#(1) saInited = localSAUnit.isInited? 1:0;
		Bit#(1) iuInited = 1;
		Bit#(1) vaInited = 1;
		
		for (DirIdx dirn=0; dirn<fromInteger(valueOf(NumPorts)); dirn=dirn+1)
		begin
			iuInited = (iuInited == 1  && inputUnits[dirn].isInited)? 1:0;
			vaInited = (vaInited == 1 && vcAllocUnits[dirn].isInited)? 1:0;
		end
		
		if (saInited==1 && iuInited==1 && vaInited==1)
		begin
			inited <= True;
		end
	endrule 

	/****************************************/
	/*********** Pipeline Stage 1 ***********/
	/****************************************/
	// Buffer Read & SA-L
	//(* descending_urgency = "rl_enqOutBufs, rl_ReqLocalSA, rl_GetLocalSARes, rl_PrepareLocalFlits, rl_deqBufs" *)
	rule rl_ReqLocalSA(inited);
		//Read necessary data
		let flits        = readFlits();
		let headers      = readHeaders();
		let saReq        = getSAReq(headers);
		let freeVCInfo   = getFreeVCInfo; //Local ports are marked as no VC
		
		//`ifdef DEBUG
		//	for(Integer inPort=0; inPort<valueOf(NumPorts) ; inPort=inPort+1)
		//	begin
		//		if (isValid(flits[inPort]))
		//		begin
		//			let flit = validValue(flits[inPort]);
		//			let header = validValue(headers[inPort]);
		//			let saR = saReq[inPort];
		//		end
		//		else begin
		//		end
		//	end
		//`endif
		
		flitsBuf <= flits;
		headersBuf <= headers;
		localSAUnit.reqSA(saReq, freeVCInfo);
	endrule
	
	rule rl_RespLocalSA(inited);
		let grantedInPorts  <- localSAUnit.getGrantedInPorts;
		let grantedOutPorts <- localSAUnit.getGrantedOutPorts;
		
		saResInBuf <= grantedInPorts;
		w_grantedOutputs <= grantedOutPorts;
	endrule


	for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort + 1)
	begin
		rule rl_GetLocalSARes(inited);
			if(saResInBuf[inPort] == 1 && isValid(flitsBuf[inPort])) begin
				let flit = validValue(flitsBuf[inPort]);
				let header = validValue(headersBuf[inPort]);
				
				`ifdef PIPELINE_DEBUG
				    $display("%t | %m | SA-L | Flit %d (R: %d,%d->%d,%d) | Input %d | Output %d | ", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, inPort, dir2Idx(header.routeInfo.nextDir));
					$display("%t | %m | Credit Local | Flit %d | Input %d | Output %d | ", $time, flit.stat.flitId, inPort, dir2Idx(header.routeInfo.nextDir));
				`endif
				
				inputUnits[inPort].deqFlit;
				// FIXME: flitId must be removed when computing area and power
			`ifdef DEBUG
              	crdUnits[inPort].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit:True, flitId: flit.stat.flitId })); 
			`else
				// FIXME: remove flitId if DEBUG flags are disabled.
              	crdUnits[inPort].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit:True})); 
			`endif
				pipe_sal_sag_flit[inPort].enq(flit);	
				pipe_grantedInputs[inPort].enq(True);
				pipe_headers[inPort].enq(headersBuf[inPort]);
			end
		endrule
	end


	for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1)
	begin
		rule rl_GetLocalSARes2(inited);
			Bool temp = w_grantedOutputs[outPort] == 1? True : False;
			pipe_grantedOutputs[outPort].enq(temp);
		endrule
	end

  /****************************************/
  /*********** Pipeline Stage 2 ***********/
  /****************************************/

  /*********** SA-G ***********/


  //Send SSR
	rule rl_SendSSR(inited);
		HeaderBundle headers;
		SARes grantedInPorts = 0;
		SARes grantedOutPorts = 0;

		for (Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort + 1) begin
			if (pipe_headers[inPort].notEmpty) begin
				headers[inPort] = pipe_headers[inPort].first;
				grantedInPorts[inPort] = pipe_grantedInputs[inPort].first ? 1 : 0;
			end
			else begin
				headers[inPort] = Invalid;
				grantedInPorts[inPort] = 0;
			end
				
			//if (pipe_grantedOutputs[inPort].notEmpty) begin
			//	grantedOutPorts[inPort] = pipe_grantedOutputs[inPort].first ? 1 : 0;
			//end
			//else begin
			//	grantedOutPorts[inPort] = 0;
			//end
		end
    	
    	let freeVCInfo = getFreeVCInfo2;
    	//let sagout = updateValidRoutingInfos(headers, grantedInPorts, freeVCInfo);
    	let sagout = updateValidRoutingInfos(headers, grantedInPorts);
		RouteInfoBundle rb = tpl_1(sagout);
		SARes newGrantedInPorts = tpl_2(sagout);
    	SmartHopsBundle sHops = getSHops(rb);
		Bool hasFreeVC = False;

		for (Integer outPort = 0; outPort < valueOf(NumNormalPorts); outPort = outPort +1) begin
			if (freeVCInfo[outPort] == 1)
			begin
	      		ssrIOUnits[outPort].putOutSSR(encodeSSR(sHops[outPort])); //Encode # of hops to one-hot code //XXX: esto tambien dispara el consumo de memoria.
			end

		end
    	
		for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort=inPort+1)
	    begin
			if (newGrantedInPorts[inPort] == 1 && pipe_sal_sag_flit[inPort].notEmpty) begin
				let rflit = pipe_sal_sag_flit[inPort].first; 
				pipe_sal_sag_flit[inPort].deq; 
				let flit = rflit;
				let header = validValue(headers[inPort]);

				`ifdef PIPELINE_DEBUG
					$display("%t | %m | SA-G | Flit %d (R: %d,%d->%d,%d) | Input %d | Output %d | ", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, inPort, dir2Idx(header.routeInfo.nextDir));
				`endif

				sa2cb[inPort].enq(SA2CB{header:header, flit: flit});
				pipe_headers[inPort].deq;
				pipe_grantedInputs[inPort].deq;
			end
	    end
	endrule
  
	// Set up flags
	for (Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1) begin
		rule rl_GlobalSA(inited);
    	//SARes grantedOutPorts = 0;
	    //let freeVCInfo = getFreeVCInfo2;
		//
		//for (Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1) begin
		//	if (pipe_grantedOutputs[outPort].notEmpty)
		//	begin
		//		grantedOutPorts[outPort] = pipe_grantedOutputs[outPort].first? 1 : 0;
		//	end
		//	else
		//	begin
		//		grantedOutPorts[outPort] = 0;
		//	end
		//	//if (freeVCInfo[outPort] == 1)
		//	//begin
		//	//	pipe_grantedOutputs[outPort].deq;
		//	//end
		//end
 
 
		//`ifdef DEBUG
		//	$display("%t | %m - rule rl_GlobalSA | grantedOutPorts: %b, freeVCInfo: %b", $time, grantedOutPorts, freeVCInfo);
		//`endif
    	////setupFlags(grantedOutPorts, freeVCInfo);

			if (outPort == valueOf(NumPorts) - 1) begin
				if (pipe_grantedOutputs[outPort].first) begin
					vcAllocUnits[outPort].doingSAG();
				end
			end
			else begin
				let hasSSR <- ssrIOUnits[outPort].hasInSSR_Requester;
				let freeVCInfo = vcAllocUnits[outPort].hasVC2;

				let newFlag = (!pipe_grantedOutputs[outPort].first    // 1. No local winner for the output port(outPort)
				&& hasSSR                       // 2. Has at least one incoming SSR
				&& freeVCInfo)?   // 3. Has a free VC at the next router
				//   -> Winner implies it has at least one free VC
				Pass:Stop;

				smartFlags[outPort].setFlag(newFlag);

				if (pipe_grantedOutputs[outPort].first || newFlag == Pass) begin
					vcAllocUnits[outPort].doingSAG();
				end
			end

		//let localPort = valueOf(NumPorts) - 1;
		//$display("Hello world: %d | grantedOutPorts: %b", localPort, grantedOutPorts);
		//if (grantedOutPorts[localPort] == 1) begin
		//	$display("Hello world");
		//	vcAllocUnits[localPort].doingSAG();
		//end
		endrule
		
		rule rl_deqPipeGrantedOutputs(inited);
	    	let freeVCInfo = getFreeVCInfo2;
			if (freeVCInfo[outPort] == 1)
			begin
				pipe_grantedOutputs[outPort].deq;
			end
		endrule
	end
   
	for(Integer outPort=0; outPort<valueOf(NumNormalPorts); outPort = outPort+1)
	begin
		rule rl_deqFlags(inited);
			smartFlags[outPort].deqFlag;
		endrule
			
	end


	/****************************/

	/*********** Local Flits ***********/
	for(DirIdx inPort=0; inPort<fromInteger(valueOf(NumPorts)); inPort = inPort+1)
	begin

		rule rl_PrepareLocalFlits(inited);
			let x = sa2cb[inPort].first;
			sa2cb[inPort].deq;

			let flit = x.flit;
			let header = x.header;
		
			`ifdef PIPELINE_DEBUG
					$display("%t | %m | ST+LT | Flit %d | Input %d | Output %d | Local", $time, flit.stat.flitId, inPort, dir2Idx(header.routeInfo.nextDir));
			`endif
			putFlit2XBar(flit, header, inPort);
		endrule
	end

	for(Integer outPort=0; outPort<valueOf(NumPorts); outPort = outPort+1)
	begin
		rule rl_enqOutBufs(inited);
			let flit <- cbSwitch.crossbarPorts[outPort].getFlit;
			outputUnits[outPort] <= flit;
	    endrule
	end


  /****************************************/
  /*********** Pipeline Stage 3 ***********/
  /****************************************/

  // Link traversals

  /***************************** Router Interface ******************************/

  /* SubInterface routerLinks 
   *  => It parameterizes the number of Flit/Credit Links.
   */
  Vector#(NumPorts, DataLink) dataLinksDummy;
  for(DirIdx prt = 0; prt < fromInteger(valueOf(NumPorts)); prt = prt+1)
  begin
	
      dataLinksDummy[prt] =
	  
        interface DataLink
          //getFlit: output side

          method ActionValue#(Flit) getFlit;
            let dstPrt = getDstPort(prt); // Isn't it the opposite
            let retFlit = ?;
            if(smartFlags[prt].isPass) begin
              retFlit = smartFlitBuf[prt];
			  // FIXME: flitId must be removed when computing area and power
			`ifdef DEBUG
              crdUnits[dstPrt].putCreditSMART(Valid(CreditSignal_{vc: retFlit.vc, isTailFlit:True, flitId: retFlit.stat.flitId})); 
			`else
				// FIXME: remove flitId if DEBUG flags are disabled
              crdUnits[dstPrt].putCreditSMART(Valid(CreditSignal_{vc: retFlit.vc, isTailFlit:True}));
			`endif

			  `ifdef PIPELINE_DEBUG
					$display("%t | %m | Credit Bypass | Flit %d | Input %d | Output %d | Local", $time, retFlit.stat.flitId, dstPrt, prt);
			  `endif
            end
            else begin
              let localFlit = outputUnits[prt];
              retFlit = localFlit;
			`ifdef DEBUG
              retFlit.stat.smartHops = retFlit.stat.smartHops + 1; //Increase smartHops
			`endif
            end
			
            `ifdef DEBUG
              retFlit.stat.hopCount = retFlit.stat.hopCount + 1; //Increase hopCount
			`endif

            let newVC <- vcAllocUnits[prt].getNextVC;
            retFlit.vc = newVC;

            retFlit.routeInfo = routingUnits[prt].smartOutportCompute(retFlit.routeInfo, prt);
            return retFlit;
          endmethod

          method Action putFlit(Flit flit) if(inited);

            if(prt == dIdxLocal) begin
			  `ifdef PIPELINE_DEBUG
				  $display("%t | %m | BW | Flit %d (R: %d,%d->%d,%d) | Input %d | Output %d | ", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, prt, -1);
			  `endif
              inputUnits[prt].putFlit(flit);
            end
            else begin
              let dstPrt = getDstPort(prt);

	      let end_dimx = (dstPrt == 1 || dstPrt == 3) && (flit.routeInfo.numXhops == 0);
	      let end_dimy = (dstPrt == 0 || dstPrt == 2) && (flit.routeInfo.numYhops == 0);
              if(smartFlags[dstPrt].isStop || end_dimx || end_dimy) begin
				`ifdef PIPELINE_DEBUG
				  $display("%t | %m | BW | Flit %d (R: %d,%d->%d,%d) | Input %d | Output %d | ", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, prt, dstPrt);
				`endif
                inputUnits[prt].putFlit(flit);
              end
              else begin
				`ifdef PIPELINE_DEBUG
					$display("%t | %m | ST+LT | Flit %d (R: %d,%d) | Input %d | Output %d | ", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, prt, dstPrt);
				`endif
                smartFlitBuf[dstPrt] <= flit; //Bypass
              end
            end
          endmethod

        endinterface;
  end 

  Vector#(NumPorts, ControlLink) controlLinksDummy;
  for(DirIdx prt = 0; prt < fromInteger(valueOf(NumPorts)); prt = prt+1)
  begin
    controlLinksDummy[prt] =
      interface ControlLink
        method ActionValue#(CreditSignal) getCredit if(inited);
          let credit <- crdUnits[prt].getCredit();
          return credit;
        endmethod

        method Action putCredit(CreditSignal creditSig) if(inited);
          if(isValid(creditSig)) begin
			`ifdef DEBUG
			 	$display("%t | %m | Credit Reception | Flit %d | Output %d | ", $time, validValue(creditSig).flitId, prt);
			`endif
            vcAllocUnits[prt].putFreeVC(validValue(creditSig).vc);
          end
        endmethod

        method Action putSSRs(SSRBundle incomingSSRs) if(inited);
			ssrIOUnits[prt].putInSSR(incomingSSRs);
        endmethod
	 
        method ActionValue#(SSR) getSSR if(inited);
			let ssr <- ssrIOUnits[prt].getOutSSR;
			return ssr;
        endmethod
      endinterface;
  end

  interface dataLinks = dataLinksDummy;
  interface controlLinks = controlLinksDummy;
  
  //method Bool isInited();
  method Bool isInited;
    return inited; 
  endmethod

endmodule
