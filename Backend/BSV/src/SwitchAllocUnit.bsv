import Vector::*;
import Fifo::*;

import Types::*;
import VirtualChannelTypes::*; 

import SwitchAllocTypes::*;
import NtkArbiter::*;

interface SwitchAllocUnit;
  method Bool isInited;
  method Action reqSA(SAReq req, FreeVCInfo freeVCInfo);
  method ActionValue#(SARes) getGrantedInPorts;
`ifdef SMART
  method ActionValue#(SARes) getGrantedOutPorts;
`endif
endinterface


// Arbiter gives the result in type Direction (Bit#(NumPorts))
// It represents the winner index in input sides.
// Ex) If a flit from north input port won the output switch toward east
//                                                     LWSEN
//       => When (outPort = dIdxEast), partialRes = 5'b00001

// By OR-ing every partial result, we get the bit representation of winners.
// Ex) If flits from N, S, W port won the switch
//                  LWSEN
//    => saRes = 5'b01101



(* synthesize *)
module mkSwitchAllocUnit(SwitchAllocUnit);

  /********************************* States *************************************/

  Reg#(Bool)                            inited             <- mkReg(False);
  //Fifo#(1, SAReq)                       saReqBuf           <- mkBypassFifo;
  //Fifo#(1, FreeVCInfo)                  freeVCInfoBuf      <- mkBypassFifo;
  //Fifo#(1, SARes)                       grantedInPortsBuf  <- mkBypassFifo;
  Wire#(SAReq)                       saReqBuf           <- mkWire;
  Wire#(FreeVCInfo)                  freeVCInfoBuf      <- mkWire;
  Wire#(SARes)                       grantedInPortsBuf  <- mkWire;
`ifdef SMART
  //Fifo#(1, SARes)                       grantedOutPortsBuf <- mkBypassFifo;
  Wire#(SARes)                       grantedOutPortsBuf <- mkWire;
`endif

  /******************************* Submodules ***********************************/
  Vector#(NumPorts, NtkArbiter#(NumPorts)) outPortArbiter  <- replicateM(mkOutPortArbiter);

  /******************************* Functions ***********************************/
  function SAReqBits genArbitReqBits(SAReq saReq, FreeVCInfo freeVCInfo);
  //Arbitration request bits are the transpose of SA request bits
  //If there is no avaialble VC, blocks the request
  //  => This increases the critical path, but it saves cycles 
  //     as it prevents failures due to VC availability after SA
	  
    SAReqBits saReqBits = newVector;

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1) begin
      for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort + 1) begin
		// FIXME: Esto es la solucion original. Necesitamos tener el siguiente VC libre
        saReqBits[outPort][inPort] = (freeVCInfo[outPort]==1)? saReq[inPort][outPort] : 0;
		// FIXME: Esto es la nueva solución en la que no es necesario tener el siguiente VC libre para pasar a SA-G
        //saReqBits[outPort][inPort] = saReq[inPort][outPort];
      end
    end
    
    return saReqBits;
  endfunction

  /************************* Initialization Behavior ***************************/
  rule doInitialize(!inited);
    inited <= True;

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
      outPortArbiter[outPort].initialize;
    end
  endrule


  //rule doSA(inited && saReqBuf.notEmpty);
  rule doSA(inited);
    //SAReq req = saReqBuf.first;
    //saReqBuf.deq;
    SAReq req = saReqBuf;

    //FreeVCInfo freeVCInfo = freeVCInfoBuf.first;
    //freeVCInfoBuf.deq;
    FreeVCInfo freeVCInfo = freeVCInfoBuf;

    SARes grantedInPorts = 0; 
    SARes grantedOutPorts = 0;

    let saReqBits = genArbitReqBits(req, freeVCInfo);
    // This loop is for debug - uncomment previous line
    //SAReqBits saReqBits = genArbitReqBits(req, freeVCInfo);
    //for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1) begin
    //  for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort + 1) begin
    //    //saReqBits[outPort][inPort] = (freeVCInfo[outPort]==1)? req[inPort][outPort] : 0;
    //    $display("%t | %m - doSA(saReqBuf.notEmpty) req: %b freeVCInfo: %b, inPort: %d, outPort: %d, freeVCInfo[outPort]: %b == 1? saReq[inPort][outPort] : 0, saReqBits[outPort][inPort]", $time, req, freeVCInfo, inPort, outPort, freeVCInfo[outPort], req[inPort][outPort], saReqBits[outPort][inPort]);
    //  end
    //end

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort +1) begin
      let partialRes <- outPortArbiter[outPort].getArbit(saReqBits[outPort]);
      grantedInPorts = grantedInPorts | partialRes;
      grantedOutPorts[outPort] = (partialRes!=0)? 1:0;
      //$display("%t | %m - doSA(saReqBuf.notEmpty) outPort: %d, grantedOutPorts[outPort]: %d, saReqBits[outPort]: %b, partialRes: %b", $time, outPort, grantedOutPorts[outPort], saReqBits[outPort], partialRes);
    end
     
    //grantedInPortsBuf.enq(grantedInPorts);
    grantedInPortsBuf <= grantedInPorts;
`ifdef SMART
    //grantedOutPortsBuf.enq(grantedOutPorts);
    grantedOutPortsBuf <= grantedOutPorts;
`endif
  endrule

  /******************************* Interface ***********************************/

  method Bool isInited = inited;

  method Action reqSA(SAReq req, FreeVCInfo freeVCInfo);
    //$display("%t | %m - reqSA(req: %h, freeVCInfo: %b)", $time, req, freeVCInfo);
    //saReqBuf.enq(req);
    //freeVCInfoBuf.enq(freeVCInfo);    
    saReqBuf <= req;
    freeVCInfoBuf <= freeVCInfo;    
  endmethod

  method ActionValue#(SARes) getGrantedInPorts;
    //grantedInPortsBuf.deq;
    //return grantedInPortsBuf.first;
    return grantedInPortsBuf;
  endmethod
`ifdef SMART
  method ActionValue#(SARes) getGrantedOutPorts;
    //grantedOutPortsBuf.deq;
    //return grantedOutPortsBuf.first;
    return grantedOutPortsBuf;
  endmethod
`endif
endmodule
