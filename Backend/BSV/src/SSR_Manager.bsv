import Vector::*;
import Fifo::*;

import Types::*;
import SmartTypes::*;

interface ManagerAndManager;
  method Action putSSRs(SSRBundle ssrSignal);
  method ActionValue#(SSRBundle) getSSRs;
endinterface

interface ManagerAndRouter;
  method Action putSSR(SSR ssr);
  method ActionValue#(SSRBundle) getSSRs;  
endinterface

interface SSR_Manager;
  interface Vector#(2, ManagerAndManager) managerChannel;
  interface Vector#(2, ManagerAndRouter)  routerChannel;
endinterface

(* noinline *)
function SSRBundle shiftSSRs(SSRBundle sb);
  SSRBundle ret = rotateR(sb);
  ret = map(decreaseSSR, ret);
  //ret[1] = ret[1] >> 1; // FIXME: hardcoded for HPCMAX = 3
  ret[0] = 0;
  return ret;
endfunction

(* noinline *)
function SSRBundle genNewSSR_Signal(SSR newSSR);
  SSRBundle ret = replicate(0);
  ret[0] = newSSR;
  return ret;
endfunction


(* synthesize *)
module mkSSR_Manager(SSR_Manager);

  //Fake buffers
  //Vector#(2, Fifo#(1, SSRBundle)) incomingSSRs <- replicateM(mkBypassFifo);
  //Vector#(2, Fifo#(1, SSR))       newSSRs      <- replicateM(mkBypassFifo);
  //Vector#(2, Fifo#(1, SSRBundle)) outgoingSSRs <- replicateM(mkBypassFifo);
  Vector#(2, Wire#(SSRBundle)) incomingSSRs <- replicateM(mkWire);
  Vector#(2, Wire#(SSR))       newSSRs      <- replicateM(mkWire);
  Vector#(2, Wire#(SSRBundle)) outgoingSSRs <- replicateM(mkWire);

  Reg#(Bool) inited <- mkReg(False);


  rule rl_init(!inited);
    inited <= True;
    for(Integer dir=0; dir<2; dir=dir+1) begin
      SSRBundle emptySSRs = replicate(0);
      //incomingSSRs[dir].enq(emptySSRs);
      incomingSSRs[dir] <= emptySSRs;
    end
  endrule


  for(Integer dir=0; dir<2; dir=dir+1) begin
    rule rl_shiftSSRs(inited);
      SSRBundle newSSR_Signal = newVector;

      //if(newSSRs[dir].notEmpty) begin // old
      if(newSSRs[dir] > 0) begin // old
      //if(newSSRs[dir].notEmpty && newSSRs[dir].first >= 1) begin // Ivan
        //newSSR_Signal = genNewSSR_Signal(newSSRs[dir].first);
        newSSR_Signal = genNewSSR_Signal(newSSRs[dir]);
        //newSSRs[dir].deq;
      end
      else begin
        //newSSR_Signal = shiftSSRs(incomingSSRs[dir].first);
	newSSR_Signal = shiftSSRs(incomingSSRs[dir]);
      end

      //incomingSSRs[dir].deq;
      //outgoingSSRs[dir].enq(newSSR_Signal);
      outgoingSSRs[dir] <= newSSR_Signal;

    endrule
    /*
    rule rl_shiftSSRs(inited);
      SSRBundle newSSR_Signal = newVector;

      //if(newSSRs[dir].notEmpty) begin // old
      if(newSSRs[dir].first > 0) begin
        newSSR_Signal = genNewSSR_Signal(newSSRs[dir].first);
      end
      else begin
	//SSRBundle ret1 = rotateR(incomingSSRs[dir].first);
	SSRBundle ret1 = incomingSSRs[dir].first;
  	SSRBundle ret = map(decreaseSSR, ret1);
  	//ret[0] = 0;
        //newSSR_Signal = shiftSSRs(incomingSSRs[dir].first);
        newSSR_Signal = ret;
      end
      
      if(newSSRs[dir].notEmpty) begin // Ivan
        newSSRs[dir].deq;
      end

      incomingSSRs[dir].deq;
      outgoingSSRs[dir].enq(newSSR_Signal);

    endrule
	*/
  end


  /* Interface between the SSR Manager and the router */
  Vector#(2, ManagerAndRouter) routerChannelDummy;
  for(Integer dir = 0; dir < 2; dir = dir+1)
  begin
    routerChannelDummy[dir] = 
      interface ManagerAndRouter
        method Action putSSR(SSR ssr) if(inited);
          //newSSRs[dir].enq(ssr);
          newSSRs[dir] <= ssr;
        endmethod 
     
        method ActionValue#(SSRBundle) getSSRs if(inited);
          //return incomingSSRs[dir].first;
          return incomingSSRs[dir];
        endmethod     
      endinterface;
  end

  Vector#(2, ManagerAndManager) managerChannelDummy;
  for(Integer dir=0; dir<2; dir=dir+1)
  begin
    managerChannelDummy[dir] = 
      interface ManagerAndManager

        method Action putSSRs(SSRBundle ssrSignal) if(inited);
          //incomingSSRs[dir].enq(ssrSignal);
          incomingSSRs[dir] <= ssrSignal;
	endmethod

        method ActionValue#(SSRBundle) getSSRs if(inited);
          //outgoingSSRs[dir].deq;
          //return outgoingSSRs[dir].first;
          return outgoingSSRs[dir];
        endmethod

      endinterface;
  end

  interface routerChannel = routerChannelDummy;
  interface managerChannel = managerChannelDummy;

endmodule
