import Vector::*;
import Types::*;
import VCAllocUnitTypes::*;
import CReg::*;

import CreditReg::*;
import VirtualChannelTypes::*;

interface SmartVCAllocUnit;
	method Bool isInited;
	method Bool hasVC;
	method Bool hasVC2;
	method Action doingSAG;
	method ActionValue#(VCIdx) getNextVC;

	method Action putFreeVC(VCIdx vc);
endinterface


(* synthesize *)
module mkSmartVCAllocUnit(SmartVCAllocUnit);

	Reg#(Bool)                 	inited     <- mkReg(False);

	Vector#(NumVCs, Reg#(int))	credit_count <- replicateM(mkReg(fromInteger(valueOf(MaxVCDepth))));
	Vector#(NumVCs, PulseWire)	pw_inc_credit_count <- replicateM(mkPulseWire);
	Vector#(NumVCs, PulseWire)	pw_dec_credit_count <- replicateM(mkPulseWire);
	PulseWire					pw_sag				<- mkPulseWire;
	Wire#(Bool)					w_VC_avail_after_sag <- mkDWire(False);

	Reg#(VCIdx)					nextVC		<- mkReg(0);


	////////////////////////////////////////////////////////////////////////
	///////////////////////////   Behaviour   //////////////////////////////
	////////////////////////////////////////////////////////////////////////

	rule initialize(!inited);
    	inited <= True;
	endrule

	rule prepareSAL(inited);
		int total_credits = 0;
		for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc + 1)
		begin
			if (pw_dec_credit_count[vc] && !pw_inc_credit_count[vc]) begin
				total_credits = total_credits + credit_count[vc] - 1;
			end
			else if (pw_inc_credit_count[vc] && !pw_dec_credit_count[vc]) begin
				total_credits = total_credits + credit_count[vc] + 1;
			end
			else begin
				total_credits = total_credits + credit_count[vc];
			end
		end
		if (pw_sag) begin
			total_credits = total_credits - 1;
		end

		w_VC_avail_after_sag <= total_credits > 0;
	endrule


	function Bool evalFreeVC ;
		Bool temp = False;
		for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc + 1)
		begin
			if (!temp &&
				(
					(credit_count[vc] == 0 && !pw_dec_credit_count[vc] && pw_inc_credit_count[vc]) ||
					(credit_count[vc] > 0 && (!pw_dec_credit_count[vc] || pw_inc_credit_count[vc])) ||
					(credit_count[vc] > 1 && pw_dec_credit_count[vc])
				)
			)
			begin
				temp = True;
			end
		end
		return temp;
	endfunction
	
	rule prepareNextVC (inited);
		VCIdx next_vc = 0;
		Bool temp = False;
		for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc + 1)
		begin
			if (!temp &&
				(
					(credit_count[vc] == 0 && !pw_dec_credit_count[vc] && pw_inc_credit_count[vc]) ||
					(credit_count[vc] > 0 && (!pw_dec_credit_count[vc] || pw_inc_credit_count[vc])) ||
					(credit_count[vc] > 1 && pw_dec_credit_count[vc])
				)
			)
			begin
				temp = True;
				next_vc = vc;
			end
		end
		nextVC <= next_vc;
	endrule


	for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc + 1)
	begin
		rule updateCreditCount (inited);
			let temp = credit_count[vc];
			if (pw_inc_credit_count[vc])
			begin
				temp = temp + 1;
			end
			if (pw_dec_credit_count[vc])
			begin
				temp = temp - 1;
			end
			`ifdef PIPELINE_DEBUG
				$display("%t | %m | Credit availability | VC: %d | %d | ", $time, vc, temp);
			`endif
			credit_count[vc] <= temp;
		endrule
	end
	

	method Bool isInited = inited;


	method Bool hasVC = w_VC_avail_after_sag;

	method Bool hasVC2 = evalFreeVC;

	method Action doingSAG;
		pw_sag.send();
	endmethod
	

	method ActionValue#(VCIdx) getNextVC;
		let vc = nextVC;
		pw_dec_credit_count[vc].send();
		return vc;
	endmethod
	

	method Action putFreeVC(VCIdx vc) if(inited);
		pw_inc_credit_count[vc].send();
	endmethod

endmodule
