import Vector::*;
import Fifo::*;
import CReg::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;


interface TrafficGeneratorBuffer;
  method Data getRemainingFlitsNumber;
  method Action putFlit(Flit flit);//, Data clkCount);
  method ActionValue#(Flit) getFlit;
endinterface

(* synthesize *)
module mkTrafficGeneratorBuffer(TrafficGeneratorBuffer);
  Wire#(Flit) tempFifo <- mkWire;
  PulseWire  pw_inc_remainingFlits <- mkPulseWire;
  PulseWire  pw_dec_remainingFlits <- mkPulseWire;
  Reg#(Data) remainingFlits <- mkReg(0);
  
  method Data getRemainingFlitsNumber = remainingFlits;


  //TODO: rule to calculate remainingFlits

  method Action putFlit(Flit flit);//, Data clkCount);
    pw_inc_remainingFlits.send();
	tempFifo <= flit;
  endmethod

  method ActionValue#(Flit) getFlit;
    pw_dec_remainingFlits.send();
	return tempFifo;
  endmethod

endmodule

