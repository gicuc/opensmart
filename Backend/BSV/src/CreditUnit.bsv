//import Fifo::*;
import FIFOF::*;
import Types::*;
import VirtualChannelTypes::*;
import CreditTypes::*;

interface ReverseCreditUnit;
  /* Credit */
  method Action putCredit(CreditSignal credit);
  `ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
  `endif
  method ActionValue#(CreditSignal) getCredit;
endinterface

(* synthesize *)
module mkReverseCreditUnit(ReverseCreditUnit);
 // Fifo#(NumVCs, CreditSignal)     creditQueue <- mkPipelineFifo; 
 // Fifo#(NumVCs, CreditSignal)     smartCreditQueue <- mkPipelineFifo;
 //Fifo#(2, CreditSignal)     creditQueue <- mkPipelineFifo; //Fixme: Para 1 VC con dos creditos no se pierde ningun paquete
 // Fifo#(2, CreditSignal)     smartCreditQueue <- mkPipelineFifo;
// FIFO#(CreditSignal)     creditQueue <- mkSizedFIFO(2); //Fixme: Para 1 VC con dos creditos no se pierde ningun paquete
// FIFO#(CreditSignal)     smartCreditQueue <- mkSizedFIFO(2); //Fixme: Para 1 VC con dos creditos no se pierde ningun paquete
// Reg#(int)					creditQueue_count <- mkReg(2);//FIXME: Overkill utilizar int para contar los huecos disponibles
// Reg#(int)					smartCreditQueue_count <- mkReg(2);//FIXME: Overkill utilizar int para contar los huecos disponibles
// PulseWire					pwIncrementCreditQueue <- mkPulseWire;
// PulseWire					pwIncrementSmartCreditQueue <- mkPulseWire;
// PulseWire					pwDecrementCreditQueue <- mkPulseWire;
// PulseWire					pwDecrementSmartCreditQueue <- mkPulseWire;
	FIFOF#(CreditSignal)     creditQueue <- mkSizedFIFOF(valueOf(MaxVCDepth)*valueOf(NumVCs)+1); //Fixme: Para 1 VC con dos creditos no se pierde ningun paquete
	FIFOF#(CreditSignal)     smartCreditQueue <- mkSizedFIFOF(valueOf(MaxVCDepth)*valueOf(NumVCs)+1); //Fixme: Para 1 VC con dos creditos no se pierde ningun paquete
	// Como dijo Enrique esto puede ser problematico ya que si un flit gana SA-L y otro toma el bypass se generan se generan dos creditos. Si esto ocurre en dos ciclos seguidos las colas se van a desbordar.
	// FIXME: Se me ocurre generar dos señales de creditos, una para SMARTCredits y otra para los creditos normales.

	//rule updateCreditQueue;
	//	int temp = creditQueue_count;
	//	if (pwIncrementCreditQueue) begin
	//		temp = temp + 1;
	//	end
	//	if (pwDecrementCreditQueue) begin
	//		temp = temp - 1; 
	//	end
	//	creditQueue_count <= temp;
	// endrule
	//
	// rule updateSmartCreditQueue;
	//	int temp = smartCreditQueue_count;
	//	if (pwIncrementSmartCreditQueue) begin
	//		temp = temp + 1;
	//	end
	//	if (pwDecrementSmartCreditQueue) begin
	//		temp = temp - 1; 
	//	end
	//	smartCreditQueue_count <= temp;
	// endrule

  method Action putCredit(CreditSignal credit);
    if(isValid(credit)) begin
      creditQueue.enq(credit);
	  //pwDecrementCreditQueue.send();
    end
  endmethod

`ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
    if(isValid(credit)) begin
      smartCreditQueue.enq(credit);
	  //pwDecrementSmartCreditQueue.send();
    end
  endmethod
`endif

  method ActionValue#(CreditSignal) getCredit;
    CreditSignal credit = ?;

    //if(creditQueue.notEmpty) begin
	//if (creditQueue_count < 2) begin
    if(creditQueue.notEmpty) begin
      creditQueue.deq;
	  //pwIncrementCreditQueue.send();
      credit = creditQueue.first();
    end
    //else if(smartCreditQueue.notEmpty) begin
	//else if (smartCreditQueue_count < 2) begin
    else if(smartCreditQueue.notEmpty) begin
      smartCreditQueue.deq;
	  //pwIncrementSmartCreditQueue.send();
      credit = smartCreditQueue.first();
    end
    else begin
      credit = Invalid;
    end

    return credit;
  endmethod

endmodule
