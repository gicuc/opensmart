import Vector::*;
//import Fifo::*;
import FIFOF::*;
import Randomizable::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;

interface TrafficGenerator;
  method Action initialize(MeshHIdx yID, MeshWIdx xID);
  method ActionValue#(Flit) getFlit;
	method Action setInjectionRate (Data ir);
endinterface

(* synthesize *)
module mkUniformRandom(TrafficGenerator);
  `ifdef DEBUG	
    Reg#(Data) clkCount  <- mkReg(0);
  `endif

  Randomize#(MeshHIdx) hIdRand	<- mkConstrainedRandomizer(0, fromInteger(valueOf(MeshHeight)-1));
  Randomize#(MeshWIdx) wIdRand	<- mkConstrainedRandomizer(0, fromInteger(valueOf(MeshWidth)-1));
  Randomize#(Data)     injRand  <- mkConstrainedRandomizer(0, 99); 
  
  //Fifo#(1, Flit) outFlit <- mkBypassFifo;
  FIFOF#(Flit) outFlit <- mkSizedFIFOF(fromInteger(valueOf(NumTrafficGeneratorBufferSlots)));

  Reg#(Bool)     startInit            <- mkReg(False);
  Reg#(Bool)     inited	              <- mkReg(False);
  Reg#(Data)     initReg              <- mkReg(0);
  Reg#(MeshWIdx) wID                  <- mkRegU;
  Reg#(MeshHIdx) hID                  <- mkRegU;
  Reg#(Data)  rInjectionRate		  <- mkReg(fromInteger(valueOf(InjectionRate)));//XXX: Para no tener que recompilar para cada InjectionRate

  rule doInitialize(!inited && startInit);
    if(initReg == 0) begin
      hIdRand.cntrl.init;
      wIdRand.cntrl.init;
      injRand.cntrl.init;
    end
    else if(initReg < zeroExtend(wID) + zeroExtend(hID)) begin
      let injRnd <- injRand.next;
      let wRnd <- wIdRand.next;
      let hRnd <- hIdRand.next;
    end
    else begin
      inited <= True;
	  `ifdef DEBUG
	    clkCount <= -3;
  	  `endif
    end
    initReg <= initReg + 1;
  endrule
  
  `ifdef DEBUG
    rule doClkCount(inited);
		clkCount <= clkCount + 1;
	endrule
  `endif

  rule genFlit(inited);
    let injVar <- injRand.next;
    //InjectionRate = {XX| injRate = 0.XX}
    //if(injVar < fromInteger(valueOf(rInjectionRate))) 
    if(injVar < rInjectionRate) 
    begin
      Flit flit = ?;
		
      flit.vc = 0;

      // FIXME: let wDest <- wIdRand.next;
      let wDest <- wIdRand.next;
	  //let wDest = 3;
      //if (wID < 3)
      //if (wID == 0)
      //begin
      //    wDest = 5;
      //end
      //if (wID > 2)
      //if (wID == 5)
      //begin
      //    wDest = 0;
      //end
      DirIdx xDir = (wDest>wID)? dIdxEast:dIdxWest;
      MeshWIdx xHops = (wDest > wID)? (wDest-wID) : (wID-wDest);

      // FIXME: let hDest <- hIdRand.next;
      let hDest <- hIdRand.next;
	  //let hDest = 0;
      DirIdx yDir = (hDest>hID)? dIdxSouth:dIdxNorth;
      MeshHIdx yHops = (hDest > hID)? (hDest-hID) : (hID-hDest);
    
      //Look-ahead routing
      //It is modified for SMART
//        flit.routeInfo.numXhops = (wDest > wID)? (wDest-wID)-1 : (wID-wDest)-1;
      flit.routeInfo.dirX = (wDest > wID)? WE_ : EW_;
      flit.routeInfo.numXhops = xHops;

      //Y-axis direction
//      let hDest <- hIdRand.next;

      //It is modified for SMART
//      flit.routeInfo.numYhops = (hDest>hID)? (hDest-hID)-1: (hID-hDest)-1;
      flit.routeInfo.dirY = (hDest > hID)? NS_ : SN_;
      flit.routeInfo.numYhops = yHops;

      //Decides initial direction
      if(wDest != wID) begin// Initial direction: X 
        flit.routeInfo.nextDir = (wDest > wID)? east_ : west_;
      end
      else if(hDest !=hID) begin //Initial direction: Y
        flit.routeInfo.nextDir = (hDest > hID)? south_:north_;
      end
      else begin
        flit.routeInfo.nextDir = local_;
	  end

      flit.flitType = HeadTail; 

	  `ifdef DEBUG
	    flit.stat.hopCount = 0;
	    flit.stat.smartHops = 0;
      	flit.stat.dstX = wDest;
      	flit.stat.dstY = hDest;
      	flit.stat.srcX = wID;
      	flit.stat.srcY = hID;
	    flit.stat.injectedCycle = clkCount;
	  `endif

      //if(flit.routeInfo.nextDir != local_) begin
      //  outFlit.enq(flit);
      //end
      outFlit.enq(flit);
    end// Injection rate if ends
  endrule


  method Action initialize(MeshHIdx yID, MeshWIdx xID) if(!inited && !startInit);
    startInit <= True;
    hID <= yID;
    wID <= xID;
  endmethod

  method ActionValue#(Flit)	getFlit if(inited);
    outFlit.deq;
    return outFlit.first;
  endmethod
	
  method Action setInjectionRate (Data ir);
		rInjectionRate <= ir;
	endmethod
endmodule



module mkBitComplement(TrafficGenerator);
  `ifdef DEBUG	
    Reg#(Data) clkCount  <- mkReg(0);
  `endif
  
  Randomize#(Data)     injRand  <- mkConstrainedRandomizer(0, 99); 
  
  //Fifo#(1, Flit) outFlit <- mkBypassFifo;
  FIFOF#(Flit) outFlit <- mkSizedFIFOF(fromInteger(valueOf(NumTrafficGeneratorBufferSlots)));

  Reg#(Bool)     startInit            <- mkReg(False);
  Reg#(Bool)     inited	              <- mkReg(False);
  Reg#(Data)     initReg              <- mkReg(0);
  Reg#(MeshWIdx) wID                  <- mkRegU;
  Reg#(MeshHIdx) hID                  <- mkRegU;
  Reg#(Data)  rInjectionRate		  <- mkReg(fromInteger(valueOf(InjectionRate)));//XXX: Para no tener que recompilar para cada InjectionRate

  rule doInitialize(!inited && startInit);
    if(initReg == 0) begin
      injRand.cntrl.init;
    end
    else if(initReg < zeroExtend(wID) + zeroExtend(hID)) begin
      let injRnd <- injRand.next;
    end
    else begin
      inited <= True;
	  `ifdef DEBUG
	    clkCount <= -3;
  	  `endif
    end
    initReg <= initReg + 1;
  endrule
	
  `ifdef DEBUG
    rule doClkCount(inited);
		clkCount <= clkCount + 1;
	endrule
  `endif

  rule genFlit(inited);
    let injVar <- injRand.next;
    //InjectionRate = {XX| injRate = 0.XX}
    //if(injVar < fromInteger(valueOf(InjectionRate))) 
    if(injVar < rInjectionRate) 
    begin
      Flit flit = ?;
		
      flit.vc = 0;

      let wDest = fromInteger(valueOf(MeshWidth)) - 1 - wID;
      let hDest = fromInteger(valueOf(MeshHeight)) - 1 - hID;

      DirIdx xDir = (wDest>wID)? dIdxEast:dIdxWest;
      MeshWIdx xHops = (wDest > wID)? (wDest-wID) : (wID-wDest);

      DirIdx yDir = (hDest>hID)? dIdxSouth:dIdxNorth;
      MeshHIdx yHops = (hDest > hID)? (hDest-hID) : (hID-hDest);
    
      //Look-ahead routing
//        flit.routeInfo.numXhops = (wDest > wID)? (wDest-wID)-1 : (wID-wDest)-1;
      flit.routeInfo.dirX = (wDest > wID)? WE_ : EW_;
      flit.routeInfo.numXhops = xHops;

      //Y-axis direction
//      let hDest <- hIdRand.next;

//      flit.routeInfo.numYhops = (hDest>hID)? (hDest-hID)-1: (hID-hDest)-1;
      flit.routeInfo.dirY = (hDest > hID)? NS_ : SN_;
      flit.routeInfo.numYhops = yHops;

      //Decides initial direction
      if(wDest != wID) begin// Initial direction: X 
        flit.routeInfo.nextDir = (wDest > wID)? east_ : west_;
      end
      else if(hDest !=hID) begin //Initial direction: Y
        flit.routeInfo.nextDir = (hDest > hID)? south_:north_;
      end
      else begin
        flit.routeInfo.nextDir = local_;
	  end

      flit.flitType = HeadTail; 
	  
	  `ifdef DEBUG
      	flit.stat.hopCount = 0;
      	flit.stat.smartHops = 0;
      	flit.stat.dstX = wDest;
      	flit.stat.dstY = hDest;
      	flit.stat.srcX = wID;
      	flit.stat.srcY = hID;
	    flit.stat.injectedCycle = clkCount;
 	  `endif


      if(flit.routeInfo.nextDir != local_) begin
        outFlit.enq(flit);
      end
    end// Injection rate if ends
  endrule


  method Action initialize(MeshHIdx yID, MeshWIdx xID) if(!inited && !startInit);
    startInit <= True;
    hID <= yID;
    wID <= xID;
  endmethod

  method ActionValue#(Flit)	getFlit if(inited);
    outFlit.deq;
    return outFlit.first;
  endmethod
  
	method Action setInjectionRate (Data ir);
		rInjectionRate <= ir;
	endmethod

endmodule
