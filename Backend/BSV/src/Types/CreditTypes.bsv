import Types::*;
import VirtualChannelTypes::*;

//Credits
typedef TMax#(ControlVCDepth, DataVCDepth)    MaxCreditCount;
typedef Bit#(TAdd#(1, TLog#(MaxCreditCount))) Credit;
typedef TMin#(ControlVCDepth, DataVCDepth)    InitialCredit; 

`ifdef DEBUG
typedef struct {
  VCIdx vc;
  Bool isTailFlit;
  Data flitId;
} CreditSignal_ deriving(Bits, Eq);
`else
typedef struct {
  VCIdx vc;
  Bool isTailFlit;
} CreditSignal_ deriving(Bits, Eq);
`endif

typedef Maybe#(CreditSignal_) CreditSignal;

