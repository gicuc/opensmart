import Vector::*;
import Fifo::*;
import CReg::*;

import Types::*;
import MessageTypes::*;
import SwitchAllocTypes::*;
import RoutingTypes::*;


interface CrossbarBufferChannel;
  method Action putFlit(Flit flit);
endinterface

interface CrossbarBuffer;
  method ActionValue#(Flit) getFlit;
  interface Vector#(NumPorts, CrossbarBufferChannel) bufferChannel;
endinterface

(* synthesize *)
module mkCrossbarBuffer(CrossbarBuffer);

  CReg#(TAdd#(NumPorts, 1), Maybe#(Flit)) buffer <- mkCReg(Invalid);

  Vector#(NumPorts, CrossbarBufferChannel) bufferChannelDummy;
  for(Integer prt=0; prt<valueOf(NumPorts); prt=prt+1) begin
    bufferChannelDummy[prt] = 
      interface CrossbarBufferChannel
        method Action putFlit(Flit flit);
          //$display("%0t - %m - CrossbarBufferChannel (putFlit) - Flit %d;  SMART remaining hops: (%d, %d); destination: (%d, %d); source: (%d, %d)", $time, flit.stat.flitId, flit.routeInfo.numYhops, flit.routeInfo.numXhops, flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
          buffer[prt] <= Valid(flit);
	    
Flit temp = validValue(buffer[valueOf(NumPorts)]); // Removeme: used for debugging
	  //$display("%0t - %m - CrossbarBufferChannel (putFlit) - Flit %d;  SMART remaining hops: (%d, %d); destination: (%d, %d); source: (%d, %d)", $time, temp.stat.flitId, temp.routeInfo.numYhops, temp.routeInfo.numXhops, temp.stat.dstY, temp.stat.dstX, temp.stat.srcY, temp.stat.srcX);
        endmethod
      endinterface;
  end

  interface bufferChannel = bufferChannelDummy;

  method ActionValue#(Flit) getFlit if(isValid(buffer[valueOf(NumPorts)]));
    buffer[valueOf(NumPorts)] <= Invalid;
    Flit temp = validValue(buffer[valueOf(NumPorts)]); // Removeme: used for debugging
  //$display("%0t - %m - CrossbarBufferChannel (putFlit) - Flit %d;  SMART remaining hops: (%d, %d); destination: (%d, %d); source: (%d, %d)", $time, temp.stat.flitId, temp.routeInfo.numYhops, temp.routeInfo.numXhops, temp.stat.dstY, temp.stat.dstX, temp.stat.srcY, temp.stat.srcX);
    return validValue(buffer[valueOf(NumPorts)]);
  endmethod

endmodule

