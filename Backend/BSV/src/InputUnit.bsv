import Vector::*;
import FIFOF::*;
import CReg::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;

import NtkArbiter::*;

import RoutingTypes::*;

interface InputUnit;
	method Bool           isInited;

	/* Flit */
	method Action         putFlit(Flit flit);
	method Maybe#(Flit)   peekFlit;
	method Action         deqFlit;

	/* Header Information */
	method Maybe#(Header) peekHeader;
endinterface

(* synthesize *)
module mkInputUnit(InputUnit);
	/********************************* States *************************************/
	Reg#(Bool)              inited <- mkReg(False);
	Wire#(Maybe#(VCIdx)) 	nextVC <- mkDWire(Invalid);

	/******************************* Submodules ***********************************/

	//Wire#(Maybe#(Flit))			arrivingFlit 	<- mkBypassWire;
	Wire#(Flit)					arrivingFlit 	<- mkWire;

	Vector#(NumVCs, Reg#(Data)) numFlits  	 	<- replicateM(mkReg(0));
	Vector#(NumVCs, PulseWire)	pwPutFlit		<- replicateM(mkPulseWire);
	Vector#(NumVCs, PulseWire)	pwDeqFlit 		<- replicateM(mkPulseWire);

	Vector#(NumVCs, FIFOF#(Flit))		vcs       <- replicateM(mkSizedFIFOF(valueOf(MaxVCDepth)+1));
	Vector#(NumVCs, FIFOF#(Header))		headers   <- replicateM(mkSizedFIFOF(valueOf(MaxVCDepth)+1));
	NtkArbiter#(NumVCs)                 vcArbiter <- mkInputVCArbiter;  //Arbiter
  
	/******************************* Functions ***********************************/
	function Bit#(NumVCs) getArbitReqBits;
		//Generate an arbitration request bit by investigating the VC queues
		Bit#(NumVCs) reqBit = 0;
		
		for (VCIdx vc=0; vc<fromInteger(valueOf(NumVCs)); vc=vc+1)
		begin
			reqBit[vc] = (numFlits[vc] == 0)? 0:1;
		end

		return reqBit;
	endfunction
	
	
	//function ActionValue#(Maybe#(VCIdx)) getNextVC;
	//actionvalue
	//	let reqBit = getArbitReqBits();
	//	let arbitRes <- vcArbiter.getArbit(reqBit);
	//	
	//	let winnerIdx = arbitRes2Idx(arbitRes);  //winner index is Invalid if there is no winner.
	//	
	//	return winnerIdx;
	//endactionvalue
	//endfunction
	
	
	function Action doArbit;
	action
		let reqBit = getArbitReqBits();
		let arbitRes <- vcArbiter.getArbit(reqBit);
		
		let winnerIdx = arbitRes2Idx(arbitRes);  //winner index is Invalid if there is no winner.
		nextVC <= winnerIdx;
	endaction
	endfunction


	/**************************** InputUnit Behavior *****************************/
	rule doInitialize(!inited);
		inited <= True;
		vcArbiter.initialize;
	endrule

  
	rule rl_loadFlit(inited);
		let flit = arrivingFlit; // The implicit condition of Wire should only fire rl_loadFlit when writting it.
		let vc = flit.vc;
		vcs[vc].enq(flit);

		if(isHead(flit)) begin
			headers[vc].enq(Header{vc: vc, routeInfo: flit.routeInfo});
		end

		`ifdef PIPELINE_DEBUG
			$display("%t | %m - rule rl_loadFlit | Flit ID: %d, Source: y_%d x_%d, Destination: y_%d x_%d, vc: %1d", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, flit.vc);
		`endif
	endrule
	
	
	rule rl_doArbit(inited);
		doArbit;
	endrule


	for (VCIdx vc = 0; vc < fromInteger(valueOf(NumVCs)); vc = vc + 1)
	begin
		rule updateNumFlits(inited);
			let temp = numFlits[vc];
			
			if (pwPutFlit[vc])
			begin
				temp = temp + 1;
			end
			
			if (pwDeqFlit[vc])
			begin
				temp = temp - 1;
			end
			numFlits[vc] <= temp;
		endrule
	end

	/******************************* Interfaces **********************************/
	method Bool isInited = inited;
		
	
	method Maybe#(Header) peekHeader if(inited);
    	if(isValid(nextVC))
		begin
			let vc = validValue(nextVC);
			return Valid(headers[vc].first);
		end
		else begin
        	return Invalid;
		end
	endmethod
	
	
	method Action putFlit(Flit flit) if(inited);
		pwPutFlit[flit.vc].send();
		//arrivingFlit <= Valid(flit);
		arrivingFlit <= flit;
		`ifdef PIPELINE_DEBUG
			$display("%t | %m - Action putFlit | Flit ID: %d, Source: y_%d x_%d, Destination: y_%d x_%d, vc: %1d, MaxVCDepth: %1d, vcs[vc]", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, flit.vc, valueOf(MaxVCDepth));
		`endif
	endmethod
	
	
	method Maybe#(Flit) peekFlit if(inited);
		
	//	if (isValid(nextVC))
	//	begin
	//		let vc = validValue(nextVC);
	//		let topFlit = vcs[vc].first;

	//		return Valid(topFlit);
	//    end
	//	else begin
    //    	return Invalid;
	//	end
		
		if (! isValid(nextVC))
		begin
			return Invalid;
		end
		else if (vcs[validValue(nextVC)].notEmpty)
		begin
			let vc = validValue(nextVC);
			let topFlit = vcs[vc].first;

			return Valid(topFlit);
		end
		else
		begin
			return Invalid;
		end
	endmethod
	
	
	method Action deqFlit if(inited && isValid(nextVC));
		`ifdef PIPELINE_DEBUG
			let flit = vcs[validValue(nextVC)].first;
			$display("%t | %m - Action deqFlit | Flit ID: %d, Source: y_%d x_%d, Destination: y_%d x_%d, isValid(nextVC): %b, nextVC: %d", $time, flit.stat.flitId, flit.stat.srcY, flit.stat.srcX, flit.stat.dstY, flit.stat.dstX, isValid(nextVC), validValue(nextVC));
		`endif
		
		if (isValid(nextVC))
		begin
        	let vc = validValue(nextVC);
			vcs[vc].deq;
			pwDeqFlit[vc].send();
			
			if(isTail(vcs[vc].first))
			begin
	        	headers[vc].deq;
       		 end
		 end
	endmethod

endmodule
