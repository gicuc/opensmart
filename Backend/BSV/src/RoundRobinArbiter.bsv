import Types::*;
import	Vector::*;
import	Fifo::*;

module mkRoundRobinArbiter#(Integer numReq)(NtkArbiter#(num));

  Reg#(Bool) inited <- mkReg(False);
  Vector#(num, Vector#(num, Reg#(Bit#(1)))) priorityBits <- replicateM(replicateM(mkReg(1)));
  Reg#(Int#(5)) rr_index <- mkReg(0);

  function Action updatePriorityBits(Int#(5) target);
  action
    Int#(5) index = target + 1;
    if (index == fromInteger(numReq))
    begin
        index = 0;
    end
    rr_index <= index;
  endaction
  endfunction

  function ActionValue#(Bool) getPermitSignal(Bit#(numReq) reqVec, Integer idx);
  actionvalue
  Bit#(numReq)	priTest = ?;
  Bit#(numReq)	priBits = ?;

  for(Integer i=0; i<numReq; i=i+1)
  begin
    priBits[i] = (i>idx)? ~priorityBits[idx][i] : priorityBits[i][idx];
  end

  priTest = priBits & reqVec;

  return (priTest == 0);
  endactionvalue
  endfunction

  function ActionValue#(Int#(5)) getGrantIdx(Bit#(num) reqVec);
  actionvalue		
  //Integer ret = ?;
  Int#(5) ret = ?;

  Int#(5) index = rr_index;
  Bool stop = False;
  for(Integer i=0; i<numReq; i=i+1)
  begin
    //let isGoodToGo <- getPermitSignal(reqVec, i);

    if(!stop && (reqVec[index] == 1)) begin
      ret = index;
      stop = True;
    end
    index = index + 1;
    if (index == fromInteger(numReq))
    begin
        index = 0;
    end
  end

  return ret;
  endactionvalue
  endfunction

  function Bit#(num)	packGrantIdx(Int#(5) idx);
    Bit#(num) ret = 0;
    ret[idx] = 1;
    return ret;
  endfunction
	
  function Action	initialize_func();
  action
    for(Integer i=0; i<numReq; i=i+1) begin
      priorityBits[i][i] <= 0;
    end
    inited <= True;
  endaction
  endfunction

  function ActionValue#(Bit#(num)) doArbit(Bit#(num) reqBit);
  actionvalue
    if(reqBit == 0) begin
      return 0;
    end
    else begin
      //Process the arbitration request
      let idx <- getGrantIdx(reqBit);
      updatePriorityBits(idx);
      let ret = packGrantIdx(idx);
      return ret;
    end
  endactionvalue
  endfunction

  method ActionValue#(Bit#(num)) getArbit(Bit#(num) reqBit);
    let ret <- doArbit(reqBit);
    return ret;
  endmethod

  method Action initialize if(!inited);
    initialize_func();
  endmethod

endmodule
